#! /bin/sh
#
# $Id: imview_post_install.sh,v 4.0 2003/04/28 14:44:14 hut66au Exp $
#
# Post-installation script for imview
#
# Hugues Talbot	16 Mar 2001
#

echo "Fixing imview installation in $1"

## 1- Fix imview.remove so that it can actually remove imview!
cat > /tmp/sedscript.$$ <<EOF
s@%%install_script_fix_destdir%%@$1@
EOF

sed -f /tmp/sedscript.$$ < imview.remove > /tmp/imview.rm.$$
mv /tmp/imview.rm.$$ imview.remove
rm /tmp/sedscript.$$
# make it executable and writable by the installer.
chmod u+wx ./imview.remove
# copy it to the right place
cp ./imview.remove $1/etc


## 2- create a shell script that sets up a few useful variables

cd $1/bin
cat > imview <<EOF
#!/bin/sh
IMVIEW_BINPATH=$1/bin
IMVIEWHOME=$1/share/Imview:${IMVIEWHOME}
export IMVIEWHOME
export IMVIEW_BINPATH
exec \${IMVIEW_BINPATH}/imview.bin \$*
EOF

# make this script executable 
chmod +x ./imview

echo "All done"

