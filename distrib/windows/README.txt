# $Id: README.txt,v 1.3 2007/06/09 01:13:54 hut66au Exp $
#
# This is the Windows-specific README file for imview.
#

This README file refers to Imview version 1.1.x and contains additional
information for Microsoft Windows.

As is usual under Windows this software is provided in binary form,
However Imview is Free Software available under the GNU Public Licence.

Get the Imview source and documentation on Sourceforge:

http://www.sourceforge.net/projects/imview/

1- Documentation

Please see the main on-line documentation accessible through

http://imview.sourceforge.net/

2- Installation files

The .lut files are Look-up Tables files in ASCII format for 
false-colour display of grey-level images. See main documentation 
for explanation and format. You can edit your own LUT files,
just put them in the installation folder for imview to see
them.

3- Restart needed after installation

Under NT-4.0 and possibly newer version of Windows, it seems a restart
(reboot) of the system is necessary for imview to become available
after install. On newer versions it may be that a logout (and log back
in) is sufficient. If you are just upgrading from an older version you
might not need to do anything.

If imview does not work for you after install or seems to work
incorrectly, try that last resort option, with all my commiseration,
sympathies, regrets and apologies.

4- Removing Imview

You can remove imview in the normal way through the Control Panel,
Add/Remove programs.

5- Bugs and improvements

I welcome bug reports and suggestions for improvements. Email the
author at

hut66au@users.sourceforge.net


Thank you for using Imview!


Imview is Copyrighted 1997-2007 Hugues Talbot
This software was installed by Inno Setup by Jordan Russell and others.
