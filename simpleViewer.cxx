/*
 * $Id: simpleViewer.cxx,v 4.1 2005/03/03 14:45:35 hut66au Exp $
 *
 * Imview, The Portable Image Analysis Application
 * Http://Www.Cmis.Csiro.Au/Hugues.Talbot/Imview
 * ----------------------------------------------------------
 *
 *  Imview Is An Attempt To Provide An Image Display Application
 *  Suitable For Professional Image Analysis. It Was Started In
 *  1997 And Is Mostly The Result Of The Efforts Of Hugues Talbot,
 *  Image Analysis Project, Csiro Mathematical And Information
 *  Sciences, With Help From Others (See The Credits Files For
 *  More Information)
 *
 *  Imview Is Copyrighted (C) 1997-2005 By The Australian Commonwealth
 *  Science And Industry Research Organisation (Csiro). Please See The
 *  Copyright File For Full Details. Imview Also Includes The
 *  Contributions Of Many Others. Please See The Credits File For Full
 *  Details.
 *
 *  This Program Is Free Software; You Can Redistribute It And/Or Modify
 *  It Under The Terms Of The Gnu General Public License As Published By
 *  The Free Software Foundation; Either Version 2 Of The License, Or
 *  (At Your Option) Any Later Version.
 *  
 *  This Program Is Distributed In The Hope That It Will Be Useful,
 *  But Without Any Warranty; Without Even The Implied Warranty Of
 *  Merchantability Or Fitness For A Particular Purpose.  See The
 *  Gnu General Public License For More Details.
 *  
 *  You Should Have Received A Copy Of The Gnu General Public License
 *  Along With This Program; If Not, Write To The Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, Ma 02111, Usa.
 * */

/*------------------------------------------------------------------------
 *
 *  A simpler viewer class, suitable for the 3D viewer
 *
 *  This time (unlike with imageViewer) I'll strive for as little coupling
 *  as necessary...
 *
 *  Hugues Talbot	 2 Mar 2005
 *
 *-----------------------------------------------------------------------*/

#include "imnmspc.hxx" // namespace definitions etc.

#include <assert.h>

#include "imview.hxx"
#include "imageIO.hxx"
#include "simpleViewer.hxx"


simpleViewer::simpleViewer(int x, int y, int w, int h, const char *label)
    : Fl_Group(x,y,w,h,label)
{
    dbgprintf("SimpleViewer constructor called with arg: (%d,%d)-(%dx%d)\n",
              x,y,w,h);

    theImage = 0;
    whichView = simpleview_top; // by default
}


simpleViewer::~simpleViewer()
{
    dbgprintf("Image Viewer destructor called\n");

    // delete all the objects
}


void simpleViewer::zapImageObject(void)
{
    if (imageObject) {
        delete imageObject;
        imageObject = 0;
    }

    return;
}


void simpleViewer::draw()
{
    dbgprintf("SimpleViewer drawing routine\n");
    fl_color(FL_BLACK);
    fl_rect(x(),y(),w(),h());
}
