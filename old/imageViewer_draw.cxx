/*------------------------------------------------------------------------
 *      
 * Huge unwieldy function, implementing only the `window-fits-the-image'
 * mode.
 *
 * Same functionality and more in current imageViewer.cxx  
 *    
 *-----------------------------------------------------------------------*/


#if 0
void imageViewer::old_draw()
{
    int imw,imh,imd;
    int i;
    int d = damage();
    bool    redrawBackground;
    bool smooth;


     if (hideMainMenu) {
         //position(x(), 0);
         Oy = 0;
     } else {
         //position(x(), MENUHEIGHT);
         Oy = MENUHEIGHT;
     }

    dbgprintf("Drawing the image NOW\n");
    
    if (d == FL_DAMAGE_ALL) {
	d = 32 ;  // 32 redraws absolutely everything...
	redrawBackground = true;
	dbgprintf("Damage is FL_DAMAGE_ALL, reduced to 32\n");
    } else {
	redrawBackground = false;
    }
    if ((d & 32) || (d & 16) || (d & 8) || (d & 4)) {

	// get the smooth preference issue sorted out
	if (fileprefs)
	    smooth = fileprefs->smoothZoomout(); // use the users' pref when available
	else
	    smooth = false;  // by default do not draw smoothly when (un)zooming
	

        // redraw parts or all of the image:
        if (imageObject) {
	    imw = currentImageWidth();
	    imh = currentImageHeight();
	    imd = currentImageDepth();

	    if (getdisplaymode() == IMV_DISPLAY_IMG_FIT_WINDOW) {
		dbgprintf("Draw to fit windows w=%d, h=%d\n",
			  w(), h());
		fl_draw_zcnb(imageObject,
			     Ox, Oy, w(), h(),
			     0, 0, imw, imh, smooth);
                xZoomFactor_ = w()/(double)imw;
                yZoomFactor_ = h()/(double)imh;
	    } else {
		// create, show or hide horizontal scrollbar if necessary
		if (imw > w()) {
		    if (!hscrollbar)
			create_hscrollbar();
		    else if (!hscrollbar->visible()) {
			hscrollbar->show();
		    }
		} else {
		    if (hscrollbar && hscrollbar->visible()) {
			hscrollbar->hide();
		    }
		}
		// create, show or hide vertical scrollbar if necessary
		if (imh > h()) {
		    if (!vscrollbar)
			create_vscrollbar();
		    else if (!vscrollbar->visible()) {
			vscrollbar->show();
		    }
		} else {
		    if (vscrollbar && vscrollbar->visible()) {
			vscrollbar->hide();
		    }
		}
	    
		int visibleW = visibleWidth();
		int visibleH = visibleHeight();

		// measure the dimensions of the actual display
		// area, taking slidebars presence into account
		// adjust positions if need be
		if (hposition_ + visibleW > imw) {
		    // hposition is actually false, probably because of a resize event!
		    hposition_ = trivmax(imw - visibleW, 0);
		}
		if (vposition_ + visibleH > imh) {
		    // hposition is actually false, probably because of a resize event!
		    vposition_ = trivmax(imh - visibleH, 0);
		}
                
		if (hscrollbar && hscrollbar->visible()) {
		    hscrollbar->scrollvalue((int)(hposition_+0.5), // horizontal position of the window
					    visibleW,   // visible width
					    0,          // by definition, horizontal origin of the image
					    imw);       // width of the image
		}
		if (vscrollbar && vscrollbar->visible()) {
		    vscrollbar->scrollvalue((int)(vposition_ + 0.5), // vertical positin of the window
					    visibleH,   // visible height
					    0,          // by definition, vertical origin of the image
					    imh);       // height of the image
		}

		// this is too annoying to left on all the time.
		//dbgprintf("Drawing image at: X=%d, Y=%d,"
		//	      "Window: Ox=%d, Oy=%d width=%d,height=%d\n",
		//          hposition_, vposition_,
		//          Ox,Oy,
		//        visibleW, visibleH);

	    
		if (d & 32 || (imw < visibleW) || (imh < visibleH)) {
		    static int oldOx = -1, oldOy = -1, oldw = -1, oldh = -1;
		
		    if ((zoomFactor() == 1.0) && useOffscreenBuff) {
			if ((imageObject->w() <= appMaxWidth)
			    && (imageObject->h() <= appMaxHeight)) {
			    // this is noticeable faster than
			    // the generic function for any zoom factor
			    // we allow
			    if ((imw >= visibleW) && (imh >= visibleH)) {
				oldw = -1; // reset the white box condition
				fl_draw_clipped(imageObject,
						Ox, Oy,
						(int)(hposition_+0.5),
						(int)(vposition_+0.5),
						visibleW, visibleH);
			    } else {
				int newOx = Ox + (visibleW - imw)/2;
				newOx = trivmax(newOx, Ox);
				int newOy = Oy + (visibleH - imh)/2;
				newOy = trivmax(newOy, Oy);
				int neww = trivmin(imw, visibleW);
				int newh = trivmin(imh, visibleH);
					    
				dbgprintf("NewOx=%d, newOy=%d\n", newOx, newOy);
				// draw a white box if necessary
				if ((newOx != oldOx) || (newOy != oldOy)
				    || (neww != oldw) || (newh != oldh)
				    || redrawBackground || (d != 32)) {
				    fl_color(FL_WHITE);
				    fl_rectf(Ox,Oy,w(),h()); // quick fill
				    fl_color(FL_BLACK);
				    fl_rect(newOx-1, newOy-1,imw+2, imh+2); 
				    oldOx = newOx;
				    oldOy = newOy;
				    oldw = neww;
				    oldh = newh;
				}
				fl_draw_clipped(imageObject,
						newOx,
						newOy,
						(int)(hposition_+0.5),
						(int)(vposition_+0.5),
						neww,
						newh);
				d = 0;
			    }
			} else {
			    // this is costing far less memory
			    if ((imw >= visibleW) && (imh >= visibleH)) {
				oldw = -1; // reset the white box condition
				fl_draw_clipped_nobuf(imageObject,
						      Ox, Oy,
						      (int)(hposition_+0.5),
						      (int)(vposition_+0.5),
						      visibleW, visibleH);
			    } else {
				int newOx = Ox + (visibleW - imw)/2;
				newOx = trivmax(newOx, Ox);
				int newOy = Oy + (visibleH - imh)/2;
				newOy = trivmax(newOy, Oy);
				int neww = trivmin(imw, visibleW);
				int newh = trivmin(imh, visibleH);
		

				dbgprintf("NewOx=%d, newOy=%d\n", newOx, newOy);
				// draw a white box if necessary
				if ((newOx != oldOx) || (newOy != oldOy)
				    || (neww != oldw) || (newh != oldh) 
				    || redrawBackground || (d != 32)) {
				    fl_color(FL_WHITE);
				    fl_rectf(Ox,Oy,w(),h()); // quick fill
				    fl_color(FL_BLACK);
				    fl_rect(newOx-1, newOy-1,imw+2, imh+2); 
				    oldOx = newOx;
				    oldOy = newOy;
				    oldw = neww;
				    oldh = newh;
				}
				fl_draw_clipped_nobuf(imageObject,
						      newOx,
						      newOy,
						      (int)(hposition_+0.5),
						      (int)(vposition_+0.5),
						      neww,
						      newh);
				d = 0;
			    }
			}
			
		    } else  {
			// this works for all zoom factors > 0.0...
			if ((imw >= visibleW) && (imh >= visibleH)) {
			    oldw = -1; // reset the white box condition
			    fl_draw_zcnb(imageObject,
					 Ox, Oy, visibleW, visibleH,
					 (int)(hposition_/zoomFactor()),
					 (int)(vposition_/zoomFactor()),
					 (int)(visibleW/zoomFactor() + 0.5),
					 (int)(visibleH/zoomFactor() + 0.5),
					 smooth);
			} else { // white box
			    int newOx = Ox + (visibleW - imw)/2;
			    newOx = trivmax(newOx, Ox);
			    int newOy = Oy + (visibleH - imh)/2;
			    newOy = trivmax(newOy, Oy);
			    int neww = trivmin(imw, visibleW);
			    int newh = trivmin(imh, visibleH);
		
			    // draw a white box if necessary
			    dbgprintf("NewOx=%d, newOy=%d\n", newOx, newOy);
			    if ((newOx != oldOx) || (newOy != oldOy)
				|| (neww != oldw) || (newh != oldh)
				|| redrawBackground || (d != 32)) {
				fl_color(FL_WHITE);
				fl_rectf(Ox,Oy,w(),h()); // quick fill
				fl_color(FL_BLACK);
				fl_rect(newOx-1, newOy-1,imw+2, imh+2); 
				oldOx = newOx;
				oldOy = newOy;
				oldw = neww;
				oldh = newh;
			    }
			
			    fl_draw_zcnb(imageObject,
					 newOx,
					 newOy,
					 neww,
					 newh,
					 (int)(hposition_/zoomFactor()),
					 (int)(vposition_/zoomFactor()), 
					 (int)(neww/zoomFactor() + 0.5),
					 (int)(newh/zoomFactor() + 0.5),
					 smooth);
			    d = 0;
			}
		    }
		    nbPointDamage = 0; // reset point semaphore
		    // reset the drawn status of lines linking point
		    PtFileMngr->resetLinesStatus();
		    // reset the drawn status of profile line when we want it sticky
		    if (angleLine && keepProfileLine)
			angleLine->drawnStatus(0);
		} 
		if (d & 16) {
		    // only redraws the top of the image (where the information box was)
		    if (zoomFactor() == 1.0) {
			fl_draw_clipped_nobuf(imageObject,
					      Ox, Oy,
					      (int)(hposition_+0.5),
					      (int)(vposition_+0.5),
					      visibleW, STATUS_HEIGHT);
		    } else if (zoomFactor() > 1.0 ) {
			fl_draw_zcnb(imageObject,
				     Ox,
				     Oy,
				     visibleW,
				     STATUS_HEIGHT,
				     (int)(hposition_/zoomFactor()),
				     (int)(vposition_/zoomFactor()),
				     (int)(visibleW/zoomFactor() + 0.5),
				     (int)(STATUS_HEIGHT/zoomFactor() + 0.5),
				     smooth);
		    } else {
			// unfortunately, in this particular situation the zoom is broken.
			// we need to redraw the whole thing
			fl_draw_zcnb(imageObject,
				     Ox, Oy, visibleW, visibleH,
				     (int)(hposition_/zoomFactor()),
				     (int)(vposition_/zoomFactor()),
				     (int)(visibleW/zoomFactor() + 0.5),
				     (int)(visibleH/zoomFactor() + 0.5),
				     smooth);

			// fl_draw_zcnb(imageObjecta,
			// 				 Ox,
			// 				 Oy,
			// 				 visibleW,
			// 				 STATUS_HEIGHT,
			// 				 xInImage(Ox),
			// 				 yInImage(Oy),
			// 				 wInImage(visibleW),
			// 				 hInImage(STATUS_HEIGHT),
			// 				 smooth);
		    }
		}

		if (d & 8) {
		    // only redraws the bottom of the image (where the information box was)
		    if (zoomFactor() == 1.0) {
			fl_draw_clipped_nobuf(imageObject,
					      Ox,
					      Oy+visibleHeight()-STATUS_HEIGHT,
					      (int)(hposition_+0.5),
					      (int)(vposition_+visibleHeight()-STATUS_HEIGHT+0.5),
					      visibleW, STATUS_HEIGHT);
		    } else {
			fl_draw_zcnb(imageObject,
				     Ox,
				     Oy+visibleHeight()-STATUS_HEIGHT,
				     visibleW,
				     STATUS_HEIGHT,
				     (int)(hposition_/zoomFactor() + 0.5),
				     (int)((vposition_+visibleHeight()-STATUS_HEIGHT)/zoomFactor() +0.5),
				     (int)(visibleW/zoomFactor() + 0.5),
				     (int)(STATUS_HEIGHT/zoomFactor() + 0.5),
				     smooth);
		    }
		}


		if (d & 4) {
		    // delete point from pointfiles
		    dbgprintf("Actually deleting point at %d,%d\n", delPtX, delPtY);
		    if (zoomFactor() == 1.0) {
			fl_draw_clipped_nobuf(imageObject,
					      (int)(Ox - hposition_ + delPtX-4 + 0.5),
					      (int)(Oy - vposition_ + delPtY-4 + 0.5),
					      delPtX - 4,
					      delPtY - 4,
					      9, 9);
		    } else if (zoomFactor() > 1.0) {
			int minx, miny;
			int wWin, hWin;
			minx = delPtX - 4;
			miny = delPtY - 4;
			wWin = 9;
			hWin = 9;
			fl_draw_zcnb(imageObject,
				     xOnWindow(minx-0.5),
				     yOnWindow(miny-0.5),
				     (int)(wWin*zoomFactor()+0.5),
				     (int)(hWin*zoomFactor()+0.5),
				     minx,
				     miny,
				     wWin,
				     hWin,
				     smooth);
		    } else {
			int minx, miny;
			int wWin, hWin;
			minx = delPtX - (int)(4/zoomFactor()+0.5);
			miny = delPtY - (int)(4/zoomFactor()+0.5);
			wWin = 9;
			hWin = 9;
			fl_draw_zcnb(imageObject,
				     xOnWindow(minx-0.5),
				     yOnWindow(miny-0.5),
				     wWin,
				     hWin,
				     minx,
				     miny,
				     (int)(wWin/zoomFactor()+0.5),
				     (int)(hWin/zoomFactor()+0.5),
				     smooth);
		    }
		    nbPointDamage--;

		}
	    }
	}
	// draws the children
	dbgprintf("Redrawing %d children\a", children());
	for (i = 0 ; i < children() ; i ++) {
	    if (child(i)->visible()) {
		dbgprintf(" %d...", i);
		child(i)->redraw();
		child(i)->draw();
	    }
	}
	dbgprintf(" done\b");
    } else if (d & 1) {      // only redraw the children that need it
	//dbgprintf("Damage(1)\n");
        dbgprintf("Only redrawing %d children\a", children());
	for (i = 0 ; i < children() ; i++) {
	    if (child(i)->visible()) {
                dbgprintf(" %d...", i);
		child(i)->redraw();
		child(i)->draw();
	    }
	}
    }

    // update the image information if possible
    if (imageInfoPanel)
	imageInfoPanel->update();
    
    return;
}

#endif // 0, obsolete code
