/*
 * $Id: saveSpect.hxx,v 4.0 2003/04/28 14:40:02 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * A save spectrum windown class
 *
 * Hugues Talbot	28 Aug 1998
 *      
 *-----------------------------------------------------------------------*/

#ifndef SAVESPECT_H
#define SAVESPECT_H

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Return_Button.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Menu.H>
#include "imview.hxx"

// prototypes for the choice menu
void spectrumformat_cb(Fl_Menu_ *, void *);

class savespect {
public:
    savespect();
    void setDefaults();
    int  doTheSaving();
    void saveFileName(const char *fn);
    const char *filenameInputValue(void) { return filenameInput->value(); }
    int selectedFileFormat(void) { return formatChoice->value();}
    void show();
    void hide();
    void toggleExtension(int format);
    void setCompression(int format);
    void setCallerType(paneltype thisPanel) {whichPanel = thisPanel;}
    friend Fl_Window *savespect_panel(savespect &s);

private:
    paneltype             whichPanel;
    // in the order in which they appear in fluid
    // -- The dialog window
    Fl_Window            *saveWindow; // main window

    // Filename group
    Fl_Group             *inputGroup;
    Fl_Input             *filenameInput;
    Fl_Button            *browseButton;

    // Choice menus
    Fl_Choice            *formatChoice;
    
    // expected buttons...
    Fl_Button            *CancelButton;
    Fl_Return_Button     *OKButton;
    
};

#endif // SAVESPECT_H
