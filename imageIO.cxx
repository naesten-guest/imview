/*
 * $Id: imageIO.cxx,v 4.16 2009/01/26 14:39:43 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * the imageIO class
 * 
 * This class allows to read/write image files, determines the image format
 * and loads the image into memory.
 *
 * At this point only RGB and grey-level images are allowed
 * the formats that are understood are:
 *    JPEG    first implemented because of extreme simplicity
 *    GIF     second implemented to explore concept of colour map
 *    TIFF    third implemented to explore data type!=char and multispectral != RGB
 *    ZIMAGE  can't do without it! very complex format (multicomponent).
 *    PNG     very useful
 *    ICS     (SCILimage) because I need it now (26 Mar 1998)
 *    Many    provided by ImageMagick
 *    META    (ITK) used by Olena and others
 *    ...
 *            
 *
 * Hugues Talbot    28 Oct 1997
 *
 * Modified Hugues Talbot 12 Dec 2007 
 *      
 *-----------------------------------------------------------------------*/



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "imunistd.h"
#include <sys/stat.h>
#include <string> // C++ strings
#include "imnmspc.hxx" // contains namespace definition, if needed
#include <map>    // STL maps.
#include "imview.hxx"
#include "nocase.hxx"
#include "server/interpreter.hxx"
#include "io/imSystem.hxx"
#include "io/readjpeg.hxx"
#include "io/readgif.hxx"
#include "io/readtiff.hxx"
#include "io/readZimage.hxx"
#include "io/readics.hxx"
#include "io/readpnm.hxx"
#include "io/readsocket.hxx"
#include "io/readraw.hxx"
#include "io/readmat5.hxx"
#include "io/readMagick.hxx"
#include "io/newprefio.hxx"
#include "io/readps.hxx"
#include "io/readpng.hxx"
#include "io/readmeta.hxx"
#include "imageIO.hxx"
#include "pointfile.hxx"
#include "imageViewer.hxx"
#include "tmpMsg.hxx"
#include "rawImage.hxx"
#include "userPrefs.hxx"

typedef std::map<string,image_psfile>::iterator mi;  // Map iterator (can't be const)

extern std::map <string, image_psfile> psfilemap;
extern std::map <string, RAWFILE_HEADER> rawfilemap;

// extern const char          *psonlyfile;
extern int                  deleteAfter;
extern imageViewer         *mainViewer;
extern imprefs             *fileprefs;
extern tmpmsg              *tmpmsgPanel;
extern rawimage            *rawImagePanel;
extern userprefs           *userprefsPanel;
extern bool                useMagick;
extern bool                forceMagick;
extern bool                forceRaw;

imageIO::imageIO()
{
    imdata = 0;
    overlay = 0;
    currBuffp = 0;
    // this is to mean that no gamma has been
    // applied yet.
    mgammas[0] = mgammas[1] = mgammas[2] = -1.0;

    minval_cache = maxval_cache = 0.0;

    // access to the image ID vector (from both GUI and server)
    semaphore_init(&access_imgvect);
    dontFreeBuffers = false; // free associated buffers when switching images.
    dbgprintf("ImageIO initialized\n");
}

imageIO::~imageIO()
{
    dbgprintf("ImageIO destroyed\n");
    delete[] imdata;
    imdata = 0;
    delete[] overlay;
    overlay = 0;
}




int imageIO::imageFormat(const char *filename)
{
/** return file format based on magic number

    RETURN VALUE:       int 

    DESCRIPTION:
    This function will open the file fname, read a few bytes and determine the
    type from these few bytes. Most image types can be worked out this way.

    HISTORY:
    Hugues Talbot       11 Sep 1997

    TESTS:

    REFERENCES:

    KEYWORDS:

**/

    FILE           *fp;
    unsigned char  magicno[HEADER_LENGTH];    /* first bytes of file */
    int            rv, n;
    static char    tmpfilename[1024], *tp;

    
#ifdef HAVE_PTHREADS // if we don't have them, no sockets either
    // If this image coming from a socket?
    if (findHeader(filename) != 0)
	// it's an incomplete check, but it will do.
        return IMAGEIO_SOCKET;
#endif

    // is this raw data read from a raw file, that the user has already
    // identified?
    if (rawfilemap.find(filename) != rawfilemap.end()) {
        return IMAGEIO_RAW;    // we'll have to perform the find again this way.
    }

    if (forceRaw) {
        rv = IMAGEIO_UNKNOWN;
        return(rv);
    }
    
    // then try our luck with SCILimage and MetaImage (mha/mhd)
    strcpy(tmpfilename, filename);
    if ((tp = strrchr(tmpfilename, '.')) != 0) {
        if ((strncmp(tp, ".ics", 4) == 0)
            || (strncmp(tp, ".ids", 4) == 0)) {
            if (access(tmpfilename, R_OK) == 0) {
                return IMAGEIO_SCILIMAGE;
            } else {
                return IMAGEIO_UNREADABLE;
            }
        }
        if ((strncmp(tp, ".mha", 4) == 0)
            || (strncmp(tp, ".mhd", 4) == 0)) {
            if (access(tmpfilename, R_OK) == 0) {
                return IMAGEIO_METAIMAGE;
            } else {
                return IMAGEIO_UNREADABLE;
            }
        }
    }
    strcat(tmpfilename, ".ics");
    if (access(tmpfilename, F_OK) == 0) {
        if (access(tmpfilename, R_OK) == 0) {
            return IMAGEIO_SCILIMAGE;
        } else {
            return IMAGEIO_UNREADABLE;
        }
    }

    

    // try the general case with magic numbers
    if (!filename)
        return IMAGEIO_NOTHERE;
    else if (!(fp = im_fopen(filename, "rb")))
        return IMAGEIO_UNREADABLE;

    n = fread(magicno, (size_t) 1, (size_t) HEADER_LENGTH, fp);  
    fclose(fp);

    if (n<4) return IMAGEIO_TOOSMALL;    /* files less than 4 bytes long... */

    rv = IMAGEIO_UNKNOWN;

    if (strncmp((char *) magicno, "CSIROZ version", (size_t)14)==0)
        rv = IMAGEIO_ZIMAGE;

#ifdef HAVE_TIFF

    else if (!forceMagick 
             && ((magicno[0]=='M' && magicno[1]=='M') 
                 || (magicno[0]=='I' && magicno[1]=='I')))
        rv = IMAGEIO_TIFF;

#endif

#ifdef HAVE_JPEG

    else if (!forceMagick 
             && (magicno[0]==0xff
                 && magicno[1]==0xd8
                 && magicno[2]==0xff))
        rv = IMAGEIO_JPEG;

#endif

#ifdef HAVE_PNG
    else if (!forceMagick
             && (magicno[1] == 'P'
                 && magicno[2] == 'N'
                 && magicno[3] == 'G'))
        rv = IMAGEIO_PNG;
#endif
    
    else if (!forceMagick 
             &&(magicno[0] == 'G'
		&& magicno[1] == 'I'
		&& magicno[2] == 'F'
		&& magicno[3] == '8'))             
        rv = IMAGEIO_GIF;

    else if (!forceMagick
             && ((magicno[0] == 'P'
                  && magicno[1] >= '1'
                  && magicno[1] <= 'B')))
	rv = IMAGEIO_PNM;

    else if (!forceMagick 
             && ((magicno[0] == '%'
                  && magicno[1] == '!')))
        rv = IMAGEIO_PS;

    else if ( (magicno[126] == 0x49 && magicno[127] == 0x4d)
              || (magicno[126] == 0x4d && magicno[127] == 0x49) )
    {
        rv = IMAGEIO_MAT5;
        if ( !((magicno[124] == 0x00 && magicno[125] == 0x01) 
               || (magicno[124] == 0x01 && magicno[125] == 0x00)) ) {
            errprintf("Found MATLAB 5 file with version other than 1: %s", 
                      filename);
            rv = IMAGEIO_UNKNOWN;
        }
    }

#ifdef HAVE_MAGICK

    else if (useMagick && testMagickImage(filename)) // can imageMagick read this image?
        rv = IMAGEIO_MAGICK;

#endif

    return rv;
}

int imageIO::read(const char *filename, int frame)
{
    int rv = 0;
    int format = imageFormat(filename);

    if (format >=0 ) {
        imageHasNoFile = false; 
        // we assume a file is attached to the image
        switch (format) {
        case IMAGEIO_JPEG:
            dbgprintf("Input image is jpeg\n");
            rv = readJpegImage(filename);
            break;
            
        case IMAGEIO_GIF:
            dbgprintf("Input image is GIF\n");
            rv = readGifImage(filename, frame);
            break;

        case IMAGEIO_TIFF:
            dbgprintf("Input image is TIFF\n");
            rv = readTiffImage(filename, frame);
            break;

        case IMAGEIO_ZIMAGE:
            dbgprintf("Input image is Z-IMAGE\n");
            rv = readZImage(filename, frame);
            break;

        case IMAGEIO_SCILIMAGE:
            dbgprintf("Input image is SCIL\n");
            rv = readICSImage(filename);
            break;

        case IMAGEIO_METAIMAGE:
            dbgprintf("Input image is META (ITK)\n");
            rv = readMETAImage(filename, frame);
            break;
            
        case IMAGEIO_PNM:
            dbgprintf("Input image is PNM\n");
            rv = readPNMImage(filename);
            break;

        case IMAGEIO_PS: 
            dbgprintf("Rendering a postscript file\n");
            rv = renderPSFile(filename, frame);
            break;

        case IMAGEIO_MAT5: 
            dbgprintf("Input iage is MAT5\n");
            rv = readMat5(filename, frame);
            break;      

        case IMAGEIO_PNG:
            dbgprintf("Input image is PNG\n");
            rv = readPNGImage(filename, frame);
            break;
            
        case IMAGEIO_SOCKET:
            dbgprintf("Input image coming from socket\n");
            rv = readSocket(filename, frame);
            if (rv == 0) {
		// This will mean extra buffers are involved, whish
		// we'll have to free when the image is manually closed
                imageHasNoFile = true;
            }
            break;

        case IMAGEIO_MAGICK:
            rv = readMagickImage(filename, frame);
            break;

        case IMAGEIO_RAW: 
            dbgprintf("Input image is raw (no header)\n");
            rv = readRaw(filename, frame); 
            break;

        case IMAGEIO_TOOSMALL:
            errprintf("Input file %s is too small to determine format\n",
                      filename);
            rv = 4;
            break;

        case IMAGEIO_NOTHERE:
            errprintf("No file name given for image input\n");
            rv = 3;
            break;

        case IMAGEIO_UNREADABLE:
            errprintf("Cannot read image %s\n"
                      "%s",
                      filename,
                      strerror(errno));
            rv = 2;
            break;

        case IMAGEIO_UNKNOWN:
            errprintf("Unrecognized image format: %s\n", filename);
            rv = 1; // things didn't go well
            break;


        default: // shouldn't happen
            errprintf("Unknown input image error\n");
            rv = 100;
            break;
        }
	// in any case, record where we are at
        currFrame = frame;
    } else
        rv = 200; // couldn't read image

    if (rv == 0) 
        flush_minmax_cache();

    return rv;
}


int imageIO::getNbFrames(const char *fname)
{
    int rv = 0;
    int format = imageFormat(fname);

    switch (format) {
    case IMAGEIO_JPEG:
        rv = jpegnbsubfiles(fname);
        break;

    case IMAGEIO_GIF:
        rv = gifnbsubfiles(fname);
        break;

    case IMAGEIO_TIFF:
        rv = tiffnbsubfiles(fname);
        break;

    case IMAGEIO_ZIMAGE:
        rv = zimagenbsubfiles(fname);
        break;

    case IMAGEIO_SCILIMAGE:
        rv = icsnbsubfiles(fname);
        break;

    case IMAGEIO_METAIMAGE:
        rv = metanbsubfiles(fname);
        break;
        
    case IMAGEIO_PNM:
        rv = pnmnbsubfiles(fname);
        break;

    case IMAGEIO_PS: {
        mi    p =  psfilemap.find(fname);
        if (p == psfilemap.end()) {
            // read the image
            read(fname);
            // try again
            if ((p =  psfilemap.find(fname)) != psfilemap.end())
                rv = tiffnbsubfiles((p->second).getfilename().c_str());
            else {
                errprintf("Cannot determine the number of pages in the document\n");
                rv = -50;
            }
        } else {
            rv = tiffnbsubfiles((p->second).getfilename().c_str());
        }
    }
        break;

    case IMAGEIO_MAT5:
        rv = mat5nbsubfiles(fname);
        break;

    case IMAGEIO_PNG:
        rv = pngnbcomp(fname);
        break;
        
    case IMAGEIO_MAGICK:
        rv = magicknbsubfiles(fname);
        break;
        
    case IMAGEIO_SOCKET:
        rv = socketnbcomp(fname);
        break;

    case IMAGEIO_RAW:
        rv = rawnbcomp(fname);
        break;

    case IMAGEIO_TOOSMALL:
        errprintf("Input file %s is too small to determine format\n",
                  fname);
        rv = -40;
        break;

    case IMAGEIO_NOTHERE:
        errprintf("No file name given for image input\n");
        rv = -30;
        break;

    case IMAGEIO_UNREADABLE:
        errprintf("Cannot read image %s\n"
                  "%s",
                  fname,
                  strerror(errno));
        rv = -20;
        break;

    case IMAGEIO_UNKNOWN:
        dbgprintf("GetNbFrames: Unrecognized image format: %s\n", fname);
        if (rawImagePanel == 0) {
            rawImagePanel = new rawimage;
            rawimage_panel(*rawImagePanel);
            // important!
            rawImagePanel->setDefaults();
        }
        rawImagePanel->setFileName(fname);
	// show the panel
        rawImagePanel->show();
        rv = -10; // impossible to know the number of frames at this point.
        dbgprintf("Unknown image format\n");
        break;


    default: // shouldn't happen
        errprintf("Unknown input image error\n");
        rv = -100;
        break;
    }

    dbgprintf("File %s has %d subimages\n",
              fname, rv);
    currImgNbFrames = rv;

    return rv;
}


bool imageIO::isOverlay(const char *filename)
{
    return (strncmp(filename, OVERLAY_MARKER, strlen(OVERLAY_MARKER)) == 0);
}

// the idea:
// - save all buffers & all images parameters
// - read an image via normal I/O (read)
// - make sure it's suitable
// - render to overlay buffer
// - re-establish saved buffer
// - display image.
// BAAD idea. Hugues Talbot  1 Mar 2001
// instead, have a readXXXOverlay for each XXX format
// 
int imageIO::readOverlay(const char *filename)
{
    int             retval = 0;
    int             format = imageFormat(filename);

    // do we have any data ?
    if (imdata) {
        switch (format) {
        case IMAGEIO_TIFF:
            retval = readTiffOverlay(filename); // sets the overlay if present
            break;
        case IMAGEIO_ZIMAGE:
            retval = readZOverlay(filename);
            break;
        case IMAGEIO_SOCKET:
            retval = readSocketOverlay(filename);
            break;

        case IMAGEIO_TOOSMALL:
            errprintf("Input file %s is too small to determine format\n",
                      filename);
            retval = 4;
            break;

        case IMAGEIO_NOTHERE:
            errprintf("No file name given for image input\n");
            retval = 3;
            break;

        case IMAGEIO_UNREADABLE:
            errprintf("Cannot read image %s\n"
                      "%s",
                      filename,
                      strerror(errno));
            retval = 2;
            break;

        case IMAGEIO_UNKNOWN:
            errprintf("Unrecognized image format: %s\n", filename);
            retval = 1; // things didn't go well
            break;
        default:
            errprintf("Unsupported image type for overlays at this time, sorry\n");
            retval = 100;
        }

    } else {
        errprintf("No image loaded\n");
        retval = 1;
    }

    return retval;
}

int imageIO::renderOverlay(const char *filename, void *p, int imspp, int start[3], int end[3], pixtype thepixtype)
{
    int w = end[0]-start[0]+1;
    int h = end[1]-start[1]+1;
    int z = end[2]-start[2]+1;
    int res = 0;


    if ((w == imageWidth())
        && (h == imageHeight())
        && (z == imageThickness())) {
        if ((thepixtype == IM_BINARY)
            ||(thepixtype == IM_INT1)
            || (thepixtype == IM_UINT1)
            || (thepixtype == IM_INT2)
            || (thepixtype == IM_UINT2)
            || (thepixtype == IM_INT4)
            || (thepixtype == IM_UINT4)) {
            if (imspp == 1) {
		// image is OK, render in buffer
                uchar *ovl = new uchar[w*h*z];

                switch (thepixtype) {
                case IM_BINARY:
                case IM_INT1:
                case IM_UINT1:
                {
                    char **b = (char **)p;
                    memcpy(ovl, b[0], w * h * z * sizeof(uchar));
                }
                break;

                case IM_INT2:
                {
                    short **b = (short **)p;
                    short  *begin, *end;
                    uchar   *ovlb;

                    ovlb = ovl;
                    begin = b[0];
                    end = begin + w * h * z;

                    while (begin != end)
                        *ovlb++ = *begin++ & 0xFF;

                }
                break;

                case IM_UINT2:
                {
                    unsigned short **b = (unsigned short **)p;
                    unsigned short  *begin, *end;
                    uchar   *ovlb;

                    ovlb = ovl;
                    begin = b[0];
                    end = begin + w * h * z;

                    while (begin != end)
                        *ovlb++ = *begin++ & 0xFF;

                }
                break;

                case IM_INT4:
                {
                    int **b = (int **)p;
                    int  *begin, *end;
                    uchar   *ovlb;

                    ovlb = ovl;
                    begin = b[0];
                    end = begin + w * h * z;

                    while (begin != end)
                        *ovlb++ = *begin++ & 0xFF;

                }
                break;

                case IM_UINT4:
                {
                    unsigned int **b = (unsigned int **)p;
                    unsigned int  *begin, *end;
                    uchar   *ovlb;

                    ovlb = ovl;
                    begin = b[0];
                    end = begin + w * h * z;

                    while (begin != end)
                        *ovlb++ = *begin++ & 0xFF;

                }
                break;

                default:
                    errprintf("Overlay image %s has an unsuitable pixel type (%s)\n",
                              filename, typeName(thepixtype));
                    res = 3;
                    break;
                }

                if (res == 0) {
                    // set the overlay in place and do the display
                    ovlspp = 1;
                    setOverlay(ovl);
                }
            } else if (imspp == 3) {
		// image is OK, render in buffer
                uchar *ovl = new uchar[3*w*h*z];
                int i;

		// overlay buffer will be arranged RGBRGBRGB...
                switch (thepixtype) {
                case IM_BINARY:
                case IM_INT1:
                case IM_UINT1:
                {
                    char **b = (char **)p;
                    char *begin, *end;
                    uchar *ovlb;

                    for (i = 0 ; i < 3  ; i++) {
                        begin = b[i];
                        end = begin + w * h * z;
                        ovlb = ovl+i;

                        while(begin != end) {
                            *ovlb = *begin++;
                            ovlb += 3;
                        }
                    }
                }
                break;

                case IM_INT2:
                {
                    short **b = (short **)p;
                    short  *begin, *end;
                    uchar   *ovlb;

                    for (i = 0 ; i < 3  ; i++) {
                        begin = b[i];
                        end = begin + w * h * z;
                        ovlb = ovl+i;

                        while(begin != end) {
                            *ovlb = *begin++ & 0xFF;
                            ovlb += 3;
                        }
                    }

                }
                break;

                case IM_UINT2:
                {
                    unsigned short **b = (unsigned short **)p;
                    unsigned short  *begin, *end;
                    uchar   *ovlb;

                    for (i = 0 ; i < 3  ; i++) {
                        begin = b[i];
                        end = begin + w * h * z;
                        ovlb = ovl+i;

                        while(begin != end) {
                            *ovlb = *begin++ & 0xFF;
                            ovlb += 3;
                        }
                    }
                }
                break;

                case IM_INT4:
                {
                    int **b = (int **)p;
                    int  *begin, *end;
                    uchar   *ovlb;

                    for (i = 0 ; i < 3  ; i++) {
                        begin = b[i];
                        end = begin + w * h * z;
                        ovlb = ovl+i;

                        while(begin != end) {
                            *ovlb = *begin++ & 0xFF;
                            ovlb += 3;
                        }
                    }
                }
                break;

                case IM_UINT4:
                {
                    unsigned int **b = (unsigned int **)p;
                    unsigned int  *begin, *end;
                    uchar   *ovlb;

                    for (i = 0 ; i < 3  ; i++) {
                        begin = b[i];
                        end = begin + w * h * z;
                        ovlb = ovl+i;

                        while(begin != end) {
                            *ovlb = *begin++ & 0xFF;
                            ovlb += 3;
                        }
                    }
                }
                break;

                default:
                    errprintf("Overlay image %s has an unsuitable pixel type (%s)\n",
                              filename, typeName(thepixtype));
                    res = 3;
                    break;
                }

                if (res == 0) {
                    // set the overlay in place and do the display
                    ovlspp = 3;
                    setOverlay(ovl);
                }
            } else {
                errprintf("Image %s is unsuitable as an overlay. Only single spectrum\n"
                          "or RGB images are acceptable\n", filename);
            }
        } else {
            errprintf("Image %s has an unsuitable pixel type to serve\n"
                      "as an overlay for the current image", filename);
            res = 2;
        }
    } else {
        dbgprintf("Overlay dimensions: %dx%d%d, image dimensions: %dx%d%d",
                  w, h, z, imageWidth(), imageHeight(), imageThickness());
        errprintf("Image %s has unsuitable dimensions  (%4dx%4dx%4d) to serve\n"
                  "as an overlay for the current image (%4dx%4dx%4d)", 
                  filename, w, h, z, imageWidth(), imageHeight(), imageThickness());
        res = 1;

    }

    return res;
}

int imageIO::write(const char *filename, int format)
{
    dbgprintf("Writing imge %s under format %d\n",
              filename, format);
    return 0;
}

/////////////////////////////////////
// access to the image representation

int imageIO::wholeImage(uchar **bufR, uchar **bufG, uchar **bufB)
{
    int nbpix = currImgWidth*currImgHeight;

    if (imspp == 1) {
        // simply return a copy of the image data
        *bufR = new uchar[nbpix];
        *bufG = 0;
        *bufB = 0;
        memcpy(*bufR, imdata, nbpix);
        return 1;
    } else if (imspp == 3) {
        uchar *ucp, *ucpr, *ucpg, *ucpb, *end;
	// we have to separate the three samples
        *bufR = new uchar[nbpix];
        *bufG = new uchar[nbpix];
        *bufB = new uchar[nbpix];

        ucp = imdata;
        end = ucp + nbpix * 3;
        ucpr = *bufR;
        ucpg = *bufG;
        ucpb = *bufB;
        while (ucp < end) {
            *ucpr++ = *ucp++;
            *ucpg++ = *ucp++;
            *ucpb++ = *ucp++;
        }

        return 3;
    } else
        return 0;
}

int imageIO::subsetImage(uchar **bufR, uchar **bufG, uchar **bufB, int &bx, int &by, int &bw, int &bh)
{
    mainViewer->getCurrentBox(bx, by, bw, bh);
    dbgprintf("Current viewed subset is: Ox=%d, Oy=%d, W=%d, H=%d\n",
              bx, by, bw, bh);

    if (imspp == 1) {
        int      j;
        uchar    *ucp, *ucpl;
        long     nbpix = bh*bw;

        *bufR = new uchar[nbpix];
        *bufG = *bufB = 0;
        for (j = 0 ; j < bh ; j++) {
            ucp = imdata + (by+j)*currImgWidth + bx;
            ucpl = *bufR + j*bw;
            memcpy(ucpl, ucp, bw);
        }
    } else if (imspp == 3) {
        int      i, j;
        uchar    *ucp, *ucpr, *ucpg, *ucpb;
        long     nbpix = bh*bw;

        *bufR = new uchar[nbpix];
        *bufG = new uchar[nbpix];
        *bufB = new uchar[nbpix];

        for (j = 0 ; j < bh ; j++) {
            ucp = imdata + 3*((by+j)*currImgWidth + bx);
            ucpr = *bufR + j*bw;
            ucpg = *bufG + j*bw;
            ucpb = *bufB + j*bw;
            for (i = 0 ; i < bw ; i++) {
                *ucpr++ = *ucp++;
                *ucpg++ = *ucp++;
                *ucpb++ = *ucp++;
            }
        }
    } else
        return 0;

    return imspp;
}

// special function for the bivariate histogram.
// take the current subset into account
// return 0 if quite alright
// we do not cope well with 3D buffers yet.
int imageIO::subsetForBH(vector<BHPoint> &v,
                         int Xcomp, int Ycomp,
                         double &minXval, double &minYval,
                         double &maxXval, double &maxYval)
{
    int bx, by, bw, bh;
    int delta, i, j;
    int retval = 0;
    double dx, dy;

    mainViewer->getCurrentBox(bx, by, bw, bh);
    dbgprintf("Current viewed subset is: Ox=%d, Oy=%d, W=%d, H=%d\n",
              bx, by, bw, bh);

    if ((Xcomp < currImgNbSamples) && (Ycomp < currImgNbSamples)) {
        int zoff = buffZOffset; // we take the current position as the default
	// empty the vector
        v.clear();
        if (currBuffp) {
            switch (currPixType) {
            case IM_BINARY:
            case IM_UINT1:
            case IM_INT1: {
                uchar *ucpX, *ucpY;
                ucpX = (uchar*)currBuffp[Xcomp] + zoff + currImgWidth*by + bx;
                ucpY = (uchar*)currBuffp[Ycomp] + zoff + currImgWidth*by + bx;
                minXval = maxXval = (double)*ucpX;
                minYval = maxYval = (double)*ucpY;
                for (j = 0 ; j < bh ; j++) {
                    ucpX = (uchar*)currBuffp[Xcomp] + zoff + currImgWidth*(by+j) + bx;
                    ucpY = (uchar*)currBuffp[Ycomp] + zoff + currImgWidth*(by+j) + bx;
                    for (i = 0 ; i < bw ; i++) {
                        dx = (double)*ucpX;
                        dy = (double)*ucpY;

                        if (dx < minXval) minXval = dx;
                        if (dy < minYval) minYval = dy;
                        if (dx > maxXval) maxXval = dx;
                        if (dy > maxYval) maxYval = dy;

                        delta = bx+i + (by+j)*currImgWidth;
                        v.push_back(BHPoint(BHPBlack,delta,dx ,dy));
                        ucpX++;
                        ucpY++;
                    }
                }
            }
                break;

            case IM_INT4:
            case IM_UINT4: {
                int *ucpX, *ucpY;
                ucpX = (int*)currBuffp[Xcomp] + zoff + currImgWidth*by + bx;
                ucpY = (int*)currBuffp[Ycomp] + zoff + currImgWidth*by + bx;
                minXval = maxXval = (double)*ucpX;
                minYval = maxYval = (double)*ucpY;
                for (j = 0 ; j < bh ; j++) {
                    ucpX = (int*)currBuffp[Xcomp] + zoff + currImgWidth*(by+j) + bx;
                    ucpY = (int*)currBuffp[Ycomp] + zoff + currImgWidth*(by+j) + bx;
                    for (i = 0 ; i < bw ; i++) {
                        dx = (double)*ucpX;
                        dy = (double)*ucpY;

                        if (dx < minXval) minXval = dx;
                        if (dy < minYval) minYval = dy;
                        if (dx > maxXval) maxXval = dx;
                        if (dy > maxYval) maxYval = dy;

                        delta = bx+i + (by+j)*currImgWidth;
                        v.push_back(BHPoint(BHPBlack,delta,dx ,dy));
                        ucpX++;
                        ucpY++;
                    }
                }
            }
                break;

            case IM_DOUBLE: {
                double *ucpX, *ucpY;
                ucpX = (double*)currBuffp[Xcomp] + zoff + currImgWidth*by + bx;
                ucpY = (double*)currBuffp[Ycomp] + zoff + currImgWidth*by + bx;
                minXval = maxXval = (double)*ucpX;
                minYval = maxYval = (double)*ucpY;
                for (j = 0 ; j < bh ; j++) {
                    ucpX = (double*)currBuffp[Xcomp] + zoff + currImgWidth*(by+j) + bx;
                    ucpY = (double*)currBuffp[Ycomp] + zoff + currImgWidth*(by+j) + bx;
                    for (i = 0 ; i < bw ; i++) {
                        dx = *ucpX;
                        dy = *ucpY;

                        if (dx < minXval) minXval = dx;
                        if (dy < minYval) minYval = dy;
                        if (dx > maxXval) maxXval = dx;
                        if (dy > maxYval) maxYval = dy;

                        delta = bx+i + (by+j)*currImgWidth;
                        v.push_back(BHPoint(BHPBlack,delta,dx ,dy));
                        ucpX++;
                        ucpY++;
                    }
                }
            }
                break;


            default:
                retval = 2;
                break;
            }
        } else {
            uchar *ucpX, *ucpY, *beg;
            // data is just char.
            beg = imdata + currImgNbSamples*(by*currImgWidth + bx);
            minXval = maxXval = (double)*(beg + Xcomp);
            minYval = maxYval = (double)*(beg + Ycomp);
            for (j = 0 ; j < bh ; j++) {
                beg = imdata + currImgNbSamples*((by+j)*currImgWidth + bx);
                ucpX = beg + Xcomp;
                ucpY = beg + Ycomp;
                for (i = 0 ; i < bw ; i++) {
                    dx = (double)*ucpX;
                    dy = (double)*ucpY;

                    if (dx < minXval) minXval = dx;
                    if (dy < minYval) minYval = dy;
                    if (dx > maxXval) maxXval = dx;
                    if (dy > maxYval) maxYval = dy;

                    delta = bx+i + (by+j)*currImgWidth;
                    v.push_back(BHPoint(BHPBlack,delta,dx ,dy));
                    ucpX += currImgNbSamples;
                    ucpY += currImgNbSamples;
                }
            }
        }

    } else
        retval = 1; // and do nothing else



    return retval;
}


int imageIO::renderPSFile(const char *filename, int frame)
{
    char                errfile[1024], panelmsg[1024];
    int                 rv = 0;
    static int          psreadres;
    mi                  p;  // map iterator
    char               *imTempPSFileName = 0;
    float              Ox, Oy, W, H, resolution, imgW, imgH;

    dbgprintf("Input image is POSTSCRIPT\n");

    // we need the user prefs panel
    if (userprefsPanel == 0) {
        userprefsPanel = new userprefs;
        userprefs_panel(*userprefsPanel);
        userprefsPanel->setDefaults();
        // no need to show it though
    }

    // see if we have rendered this file already
    if (((p = psfilemap.find(filename)) == psfilemap.end()) 
        || ((p->second).gethash() != userprefsPanel->getPsPrefsHash())) { 

        if (p != psfilemap.end()) {
            dbgprintf("Re-rendering postscript file\n");
            // destroying previous cache file
            string cache = (p->second).getfilename();
            dbgprintf("Removing previous cache file %s\n", cache.c_str());
            unlink(cache.c_str());
        } else
            dbgprintf("Rendering postscript file (for the first time)\n");

        imTempPSFileName = tempnam(0, "flim");
        char  gscommand[1024];

        strcpy(errfile, imTempPSFileName);
        strcat(errfile, ".err");

        if (!tmpmsgPanel) {
            tmpmsgPanel= new tmpmsg;
            tmpmsg_panel(*tmpmsgPanel);
        }

        psinfo mypsinfo(filename);

        sprintf(panelmsg, 
                "Calling GhostScript interpreter on file\n"
                "%s\n"
                "This may take a while",
                filename);
        tmpmsgPanel->setMessage(panelmsg);
        tmpmsgPanel->show();
	// this trick below is to ensure that the message on
	// the warning panel is displayed.
        for (int w = 0 ; w < 3 ; w++)
            Fl::wait(0.05); // respond to events for a few milliseconds

        // parsing the file gets the bounding box and fixes a few things
        // such as making sure a showpage is present, etc.
        mypsinfo.parsefile();
        mypsinfo.getBoundingBox(Ox, Oy, W, H);
        resolution = fileprefs->psRenderRes();
        imgW = (W * resolution)/DEFAULT_PS_RES + 0.5 ;
        imgH = (H * resolution)/DEFAULT_PS_RES + 0.5 ;

        sprintf(gscommand,
                "%s -q -dNOPAUSE -dBATCH -dMaxBitmap=50000000 -sDEVICE=%s "
                "-dTextAlphaBits=%u -dGraphicsAlphaBits=%u -g%dx%d -r%d -sOutputFile=%s %s -c quit 2> %s",
                fileprefs->gspath(),
                (fileprefs->psRenderDepth() == PSREND_BW) ? "tiffg4":"tiff24nc",
                (fileprefs->psAntialiasRender()) ? 4:1, 
                (fileprefs->psAntialiasRender()) ? 4:1,
                (int)imgW, (int)imgH,
                (int)(resolution + 0.5),
                imTempPSFileName,
                mypsinfo.getTmpFilename(), // use the temporary file made when parsing
                errfile);



        dbgprintf("executing:\n%s\n", gscommand);
	// call the system for rendering the file to an image
	// maybe this should be done in a separate thread...
        psreadres = system(gscommand);
        if (psreadres == 0)
            rv = read(imTempPSFileName, frame);         // OK, recursive read!
        else {
            errprintf("Failed to run the Ghostscript interpreter correctly");
            rv = 5;
        }

        tmpmsgPanel->hide();

    } else {
	// read the temp file
        dbgprintf("Using pre-rendered file %s\n", (p->second).getfilename().c_str());
        rv = read((p->second).getfilename().c_str(), frame);
    }

    if (rv != 0) {  // could not read
        struct stat statbuf;
        if (imTempPSFileName) {
            // erase the temp file and delete the temporary storage
            unlink(imTempPSFileName);
            free(imTempPSFileName);
            imTempPSFileName = 0;
        }
	// delete the faulty map entry
        if (p != psfilemap.end()) {
            psfilemap.erase(p);
        }
        if ((stat(errfile, &statbuf) == 0) && (statbuf.st_size > 0)) {
            errprintf("Postscript rendering of % failed\n"
                      "Error log to be found in file %s\n",
                      filename, errfile);
        }
    } else {
        if (imTempPSFileName)  {
            // everything went well, save the temporary name to the map
            string tempps(imTempPSFileName);
            string currenthash = userprefsPanel->getPsPrefsHash();
            image_psfile rendercache(tempps, currenthash);
            psfilemap[filename] = rendercache; // this makes a copy
            free(imTempPSFileName);
            imTempPSFileName = 0;
        } // else the temp file is already in the map

	// set the zoom factor and the smooth flag
        double renderes = fileprefs->psRenderRes();
        double dspres = fileprefs->psDisplayRes();
        double zoomf;
        if (renderes != 0)
            zoomf = dspres/renderes;
        else
            zoomf = 1.0;
        mainViewer->setDefaultZoomFactor(zoomf);
        if (fileprefs->psRenderSmooth())
            fileprefs->smoothZoomout(true);
        else
            fileprefs->smoothZoomout(false);
	// destroy the file containing the errors
        unlink(errfile);
    }

    return rv;
}   

// messaging GUI <-> server

// This is a message FROM the server TO the GUI
void imageIO::pleaseDisplay(const string &s)
{
    semaphore_down(&access_imgvect);
    dbgprintf("Ok, will display %s later on\n", s.c_str());
    imgToDisplay.push_back(s);
    semaphore_up(&access_imgvect);
}

// this is the GUI checking on the messages
int imageIO::checkDisplay(string &as)
{
    int retval = 0;

    semaphore_down(&access_imgvect);
    if (!imgToDisplay.empty()) {
        as = imgToDisplay.front();
        dbgprintf("Aha: image %s is to be displayed!\n", as.c_str());
	// effectively an inefficient 'pop_front'
        imgToDisplay.erase(imgToDisplay.begin());
        retval = 1;
    }
    semaphore_up(&access_imgvect);

    return retval;
}

#ifdef HAVE_PTHREADS // surrogate for HAVE_SERVER

// find header based on name
IMAGE_HEADER *imageIO::findHeader(const char *name)
{
    IMAGE_HEADER *res ;

    semaphore_down(&access_imgvect);
    res = put::find_header(name);
    semaphore_up(&access_imgvect);

    return res;
}

// find header based on id
IMAGE_HEADER *imageIO::findHeader(unsigned int id)
{
    IMAGE_HEADER *res ;

    semaphore_down(&access_imgvect);
    res = put::find_header(id);
    semaphore_up(&access_imgvect);

    return res;
}

// delete header based on ID
void imageIO::closeHeader(unsigned int id)
{
    semaphore_down(&access_imgvect);
    put::delete_header(id);
    semaphore_up(&access_imgvect);
}

// delete header based on name
void imageIO::closeHeader(const char *fname)
{
    semaphore_down(&access_imgvect);
    put::delete_header(fname);
    semaphore_up(&access_imgvect);
}

#else
// we fake 'em, we still need the definitions
// find header based on name
IMAGE_HEADER *imageIO::findHeader(const char *)
{
    return 0;
}

// find header based on id
IMAGE_HEADER *imageIO::findHeader(unsigned int)
{
    return 0;
}

// delete header based on ID
void imageIO::closeHeader(unsigned int)
{
}

// delete header based on name
void imageIO::closeHeader(const char *)
{
}

#endif
