/*
 * $Id: profileBox.hxx,v 4.0 2003/04/28 14:39:59 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * The profile display panel
 *
 * Hugues Talbot	 1 Aug 1998
 *      
 *-----------------------------------------------------------------------*/

#ifndef PROFILEBOX_H
#define PROFILEBOX_H

#include <stdlib.h>
#include <stdio.h>

#include <FL/Fl.H>
#include <FL/x.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Return_Button.H>
#include <FL/Fl_Output.H>
#include <FL/fl_ask.H>
#include "axisBox.hxx"

#define WIDTHMARGIN 4
#define HEIGHTMARGIN 8

#ifdef WIN32
#  define MY_CURSOR_VLINE FL_CURSOR_HAND
#else
#  define MY_CURSOR_VLINE 58
#endif

class myProfileBox : public Fl_Box {
public:
    myProfileBox(int x, int y, int w, int h, const char *l=0)
        : Fl_Box(x,y,w,h,l) {
	profileR = profileB = profileG = 0;
	nbProfileValues = 0;
	mins = maxs = 0;
    }
    int  handle(int event); // we need this for mouse control
    void handleButtonPushed();
    void handleButtonDragged();
    void handleButtonReleased();
    void setRelative() {absolute = false; recomputeLimits(); redraw();}
    void setAbsolute() {absolute = true; recomputeLimits(); redraw();}
    void setProfile(class profile *s) {theProfile = s;}
    void setProfiles(int startX, int startY, int endX, int endY,
		     double *theProfileR, double *theProfileG,
		     double *theProfileB, int nbval);
    double *getProfileR() {return profileR;}
    double *getProfileG() {return profileG;}
    double *getProfileB() {return profileB;}
    int     getNbValues() {return nbProfileValues;}
    void    recomputeLimits();
    void    draw();

private:
    double  *profileR, *profileG, *profileB, maxs, mins;
    int      nbProfileValues;
    int      sx, sy, ex, ey;
    bool     absolute;
    class profile *theProfile;
};

class profile {
public:
    profile();
    ~profile();
    void setDefaults();
    void setData(double *R,
		 double *G,
		 double *B,
		 int nb,
		 int xstart, int ystart,
		 int xend, int yend);
    
    double *getDataR() {return profileBox->getProfileR();}
    double *getDataG() {return profileBox->getProfileG();}
    double *getDataB() {return profileBox->getProfileB();}
    
    int  getNbVal() {return profileBox->getNbValues();}
    int  getXstart() {return xS;}
    int  getYstart() {return yS;}
    int  getXend() {return xE;}
    int  getYend() {return yE;}
    
    int  visible();
    void redraw();
    void show();
    void hide();
    void displayRelative(void) {profileBox->setRelative();}
    void displayAbsolute(void) {profileBox->setAbsolute();}
    void setXValue(int xv);
    void setYValue(int yv);
    void setZValue(int zv);
    void setRValue(double Rv);
    void setGValue(double Gv);
    void setBValue(double Bv);
    void setAxisBoxesLimits(double xminlimit, double xmaxlimit,
			    double yminlimit, double ymaxlimit);
    friend Fl_Double_Window *profile_panel(profile &s);

private:
    int                     xS, yS, xE, yE;
    Fl_Double_Window               *profileWindow;
    myProfileBox            *profileBox;
    axisBox                 *abcissaBox, *ordinateBox;
    Fl_Button               *saveButton, *printButton;
    Fl_Return_Button        *OKButton;
    Fl_Output               *profileTitle;
    Fl_Output               *xvalue, *yvalue, *zvalue;
    Fl_Output               *Rvalue, *Gvalue, *Bvalue;
    Fl_Check_Button         *absolute, *relative;
    Fl_Check_Button         *keepline;
    
};


#endif // PROFILEBOX_H
