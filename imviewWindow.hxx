/*------------------------------------------------------------------------
 *
 *
 * A subclass of the Fl_Window, that has a few nicer features
 *
 * Hugues Talbot	 5 Jan 2001  
 * 
 * Modified 24 Feb 2005: now a subclass of Fl_Overlay_Window to cope with 
 *                       Mac OS/X Quartz rendering of overlays.
 *      
 *-----------------------------------------------------------------------*/

#ifndef IMVIEWWINDOW_H
#define IMVIEWWINDOW_H


#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Overlay_Window.H>
#ifdef HAVE_XINERAMA
#  include <FL/Fl.H>
#  include <FL/x.H>
extern "C" {
#  include <X11/extensions/Xinerama.h>
}
#endif // HAVE_XINERAMA

class imviewWindow : public
#ifdef IMVIEW_USES_DBLBUF
    Fl_Overlay_Window
#else
    Fl_Window
#endif
{
public:
    imviewWindow(int w, int h, const char *label = 0);
    imviewWindow(int x, int y, int w, int h, const char *label=0);
    ~imviewWindow();
    void removeMainMenu(void);
    void addMainMenu(void);
    void resize(int x,int y, int w, int h);
    void size_range(int minw, int minh, int maxw=0, int maxh=0, int dw=0, int dh=0, int aspect=0);
    int  currentDisplayWidth(void);
    int  currentDisplayHeight(void);
    void setZoomSelRect(int x, int y, int w, int h) {
        dbgprintf("Calling setZoomSelRect\n");
    	zoomSelRect.x = x ; zoomSelRect.y = y;
    	zoomSelRect.w = w ; zoomSelRect.h = h;
    }
    void draw_overlay(void);
#ifdef HAVE_XINERAMA
    // redefine these non-virtual methods with a version that supports Xinerama, if present
    void fullscreen(void);
    // fullscreen_off from stock fltk is still compatible.
#endif

private:
    int           w_lower_limit, w_upper_limit;
    int           h_lower_limit, h_upper_limit;
    SELRECT       zoomSelRect;
#ifdef HAVE_XINERAMA
    int  xinerama_screen;
    XineramaScreenInfo *screens;
    void  getXineramaInfo(void);
#endif
};

#endif // IMVIEWWINDOW_H
