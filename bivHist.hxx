/*
 * $Id: bivHist.hxx,v 4.0 2003/04/28 14:39:41 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * the bivariate histogram panel, for multispectra images.
 *
 * Hugues Talbot	26 Apr 1999
 *      
 *-----------------------------------------------------------------------*/

#ifndef BIVHIST_H
#define BIVHIST_H

#include <vector> // STL vectors.
#include "imnmspc.hxx" // namespace def. if required

#include <FL/Fl.H>
#include <FL/x.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Scroll.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Return_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Roller.H>
#include <FL/Fl_Value_Output.H>
#include <FL/fl_ask.H>

#include "imview.hxx"
#include "axisBox.hxx"

#define  BHP_MARGIN 4

using std::vector;

// define an iterator on vectors of BHPoints
typedef vector<BHPoint>::iterator bhpointIterator;


// the `bivariate histogram canvas'
class bivHistBox: public Fl_Box {
public:
    bivHistBox(int x, int y, int w, int h, const char *l=0);
    int handle(int event); // we need this for mouse control
    int handleMouseMoved(double &, double &);
    int handleMouseButtonPushed(void);
    void draw(); // we need to re-implement this.
    void drawPointWithJitter(double xval, double yval, BHPColour colour);
    void setData(int xcomponent, int ycomponent);
    void workoutCorrection(void);
    void limits(double &minX, double &minY, double &maxX, double &maxY) {
	minX = minXval;
	minY = minYval;
	maxX = maxXval;
	maxY = maxYval;
    }
    void setParentPanel(class bivhist *it) {parentPanel = it;}
private:
    vector <BHPoint>   allPoints;         // we will push_back all the points that come through.
    class bivhist      *parentPanel;
    double             minXval, maxXval;  // for the X and Y limits 
    double             minYval, maxYval;
    double             xCorrection, yCorrection;
    bool               dataIsInteger;
    // original dimensions.
    int                xo, yo, wo, ho;
    
};

// the GUI interface
class bivhist {
public:
    bivhist();
    ~bivhist(); 
    void              setDefaults();
    void              show();
    void              hide();
    void              computeNewBH();
    void              setPosition(char *p) {positionOutput->value(p);}
    BHPColour         getCurrentColour(void) {return (BHPColour)regioncolourChoice->value();}
    double            getPenWidth(void) {return penWidthValue->value();}
    void              setPenWidth(double v) { penWidthValue->value(v);}
    void              setTransparency(double v); 
    void              makeCurrent(void) {bivhistWindow->make_current();}
    bool              selectRegionIsOn(void) {return (selectRegionButton->value() != 0);}
    void              enlargeHisto(void);
    void              shrinkHisto(void);
    void              fitHisto(void);
    void              getScrollClip(int &x, int &y, int &w, int &h) {
	x = bivhistScroll->x();
	y = bivhistScroll->y();
	h = bivhistScroll->h();
	w = bivhistScroll->w();
    }
    friend Fl_Window *bivhist_panel(bivhist &bh);

private:
    bivHistBox        *histoBox; // where the drawing will occur

    // all of these declared in fluid
    Fl_Window         *bivhistWindow;
    Fl_Scroll         *bivhistScroll;
    Fl_Button         *selectRegionButton;
    Fl_Button         *clearRegionButton, *clearAllRegionsButton;
    Fl_Button         *zoominButton, *zoomoutButton, *resetzoomButton;
    Fl_Button         *dismissButton;
    Fl_Button         *printButton, *saveButton;
    Fl_Return_Button  *computeButton;
    Fl_Input          *Xinput, *Yinput;
    Fl_Output         *positionOutput;
    Fl_Roller         *penRadiusRoller, *transparencyRoller;
    Fl_Value_Output   *penWidthValue, *transparencyValue;
    axisBox           *absissaBox, *ordinateBox;
    Fl_Choice         *regioncolourChoice;
};


#endif // BIVHIST_H
