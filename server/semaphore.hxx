/*
 * $Id: semaphore.hxx,v 4.0 2003/04/28 14:44:42 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/****************************************************************************\
*                                       
*                                Written by
*                     Tom Wagner (wagner@cs.umass.edu)
*                  at the Distributed Problem Solving Lab
*       Department of Computer Science, University of Massachusetts,
*                            Amherst, MA 01003
*                                     
*        Copyright (c) 1995 UMASS CS Dept. All rights are reserved.
*                                     
*           Development of this code was partially supported by:
*                        ONR grant N00014-92-J-1450
*                         NSF contract CDA-8922572
*                                      
* ---------------------------------------------------------------------------
* 
* This code is free software; you can redistribute it and/or modify it.
* However, this header must remain intact and unchanged.  Additional
* information may be appended after this header.  Publications based on
* this code must also include an appropriate reference.
* 
* This code is distributed in the hope that it will be useful, but 
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
* or FITNESS FOR A PARTICULAR PURPOSE.
* 
\****************************************************************************/

#ifndef SEMAPHORES_H
#define SEMAPHORES_H

#ifdef HAVE_PTHREADS
#include <pthread.h>

typedef struct Semaphore
{
    int         v;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
}
Semaphore;




int         semaphore_down (Semaphore * s);
int         semaphore_decrement (Semaphore * s);
int         semaphore_up (Semaphore * s);
void        semaphore_destroy (Semaphore * s);
void        semaphore_init (Semaphore * s);
int         semaphore_value (Semaphore * s);
int         tw_pthread_cond_signal (pthread_cond_t * c);
int         tw_pthread_cond_wait (pthread_cond_t * c, pthread_mutex_t * m);
int         tw_pthread_mutex_unlock (pthread_mutex_t * m);
int         tw_pthread_mutex_lock (pthread_mutex_t * m);
#endif

#endif //SEMAPHORE_H

