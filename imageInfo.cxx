/*
 * $Id: imageInfo.cxx,v 4.2 2003/09/11 12:09:12 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * An image info panel using fluid
 *
 * Hugues Talbot	 2 May 1998
 *      
 *-----------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include "imview.hxx"
#include "imageIO.hxx"
#include "imageViewer.hxx"
#include "menubar.hxx"
#include "imageInfo.hxx"

extern char        *imgDesc;
extern imageIO     *IOBlackBox;
extern imageViewer *mainViewer;



imageinfo::imageinfo()
{
    dbgprintf("Image info panel constructed\n");
    // make sure this is done, fluid depends on it...
    outputWindow = 0;
    return;
}


void imageinfo::setDefaults()
{
    dbgprintf("Setting defaults for info panel\n");
    textOutput->value("We have no information at this stage...\n");

    return;
}

void imageinfo::update(bool forced)
{
    static char buffer[2048];
    static char tmpbuf[100];

    if (outputWindow && (outputWindow->visible() || forced)) {
	if (IOBlackBox->imageData() == 0) {
	    sprintf(buffer, "No image present\n");
	} else {
	    int x, y, w, h;
	    sprintf(buffer, "Path to image: %s\n", IOBlackBox->getImName());
	    int spp = IOBlackBox->getCurrImgNbSamples();
	    sprintf(tmpbuf, "Size: %dx%dx%d, %d sample%c\n",
		    IOBlackBox->imageWidth(),
		    IOBlackBox->imageHeight(),
		    IOBlackBox->imageThickness(),
		    spp, (spp > 1) ? 's':' '
		);
	    strcat(buffer, tmpbuf);
	    sprintf(tmpbuf, "Format: %s\n", IOBlackBox->getImgDesc());
	    strcat(buffer, tmpbuf);
	    sprintf(tmpbuf, "Pixel type: %s\n", IOBlackBox->typeName(IOBlackBox->getCurrImgPixType()));
	    strcat(buffer, tmpbuf);
	    sprintf(tmpbuf, "Image type: %s\n", IOBlackBox->imgTypeName(IOBlackBox->getCurrImgImgType()));
	    strcat(buffer, tmpbuf);
	    int xO, yO, zO;
	    xO = IOBlackBox->getXOffset();
	    yO = IOBlackBox->getYOffset();
	    zO = IOBlackBox->getZOffset();
	    sprintf(tmpbuf, "Offsets: xO=%d, yO=%d, zO=%d\n",
		    xO, yO, zO);
	    strcat(buffer, tmpbuf);
	    
            if (mainViewer->getdisplaymode() == IMV_DISPLAY_IMG_FIT_WINDOW) {
                sprintf(tmpbuf, "Current X zoom factor: %f\n", mainViewer->xZoomFactor());
                strcat(buffer, tmpbuf);
                sprintf(tmpbuf, "Current Y zoom factor: %f\n", mainViewer->yZoomFactor());
                strcat(buffer, tmpbuf);
                sprintf(tmpbuf, "Aspect ratio: %f\n", mainViewer->xZoomFactor()/mainViewer->yZoomFactor());
                strcat(buffer, tmpbuf);
            } else {
                sprintf(tmpbuf, "Current zoom factor: %f ", mainViewer->zoomFactor());
                strcat(buffer, tmpbuf);
                sprintf(tmpbuf, "Default zoom factor: %f \n", mainViewer->getDefaultZoomFactor());
                strcat(buffer, tmpbuf);
                sprintf(tmpbuf, "Minimum zoom factor: %f \n", mainViewer->getMinZoomFactor());
                strcat(buffer, tmpbuf);
            }
	    mainViewer->getCurrentBox(x,y,w,h);
	    sprintf(tmpbuf, "Current subset: x=%d y=%d z=%d w=%d h=%d\n",
		    x, y, IOBlackBox->getCurrZpos(),
		    w, h);
	    strcat(buffer, tmpbuf);
	    sprintf(tmpbuf, "Frame: %d/%d\n",
		    IOBlackBox->getCurrImgFrame()+1,
		    IOBlackBox->getCurrImgNbFrames());
	    strcat(buffer, tmpbuf);
	}
	if (textOutput)
	    textOutput->value((const char *)buffer);
    }
	
    return;
}

void imageinfo::show()
{
    update(true);
    outputWindow->show();
}

void imageinfo::hide()
{
    outputWindow->hide();
}


// callbacks associated with the dialog

void okbutton_cb(Fl_Return_Button *, imageinfo *panel)
{
    dbgprintf("Dismiss Button pressed on image info panel\n");
    // hide the window
    panel->hide();
    return;
}
