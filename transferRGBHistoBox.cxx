/*
 * $Id: transferRGBHistoBox.cxx,v 4.0 2003/04/28 14:40:20 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * A subclass of Box for drawing transfer functions
 *
 * Hugues Talbot	 3 May 1998
 *      
 *-----------------------------------------------------------------------*/

#include <math.h>

#include "imview.hxx"
#include "imageIO.hxx"
#include "imageViewer.hxx"
#include "transferRGBFunction.hxx"
#include "transferRGBFunctionBox.hxx"
#include "transferRGBHistoBox.hxx"

extern imageIO *IOBlackBox;
extern imageViewer *mainViewer;
extern transferRGB  *transferRGBEditorPanel;

void myTransferRGBHistoBox::makeHisto()
{
    int i, spp, val;
    long nbpix;
    unsigned char *p, *end;
    
    for (i = 0 ; i < 256 ; i++) {
	theRGBHisto[i][0] = 0;
	theRGBHisto[i][1] = 0;
	theRGBHisto[i][2] = 0;
    }

    nbpix = IOBlackBox->imageWidth() * IOBlackBox->imageHeight();
    spp = IOBlackBox->imageDepth();
    
    if (spp == 1) {
	p = IOBlackBox->imageData();
	end = p + nbpix;

	while (p != end) {
	    val = *p++;
	    theRGBHisto[val][0]++; // simple.
	    theRGBHisto[val][1]++;
	    theRGBHisto[val][2]++;
	}
    } else if (spp == 3) {
	// we need to be a bit more conservative with colour
	p = IOBlackBox->imageData();
	end = p + spp * nbpix;

	while (p != end) {
	    theRGBHisto[*p++][0]++; // simple as well.
	    theRGBHisto[*p++][1]++;
	    theRGBHisto[*p++][2]++;
	}
	
    }

    maxval = 0;
    for (i = 0 ; i < 256 ; i++) {
	for (int j = 0 ; j < 3 ; ++j) {
	    if (theRGBHisto[i][0] > maxval)
		maxval= theRGBHisto[i][0];
	    if (theRGBHisto[i][1] > maxval)
		maxval= theRGBHisto[i][1];
	    if (theRGBHisto[i][2] > maxval)
		maxval= theRGBHisto[i][2];
	}
    }
    
    dbgprintf("Max val in histo: %d\n", maxval);
	
    return;
}

void myTransferRGBHistoBox::draw()
{
    double x0[3], y0[3], x1[3], y1[3];
    int xx;
    int drawChannel[3];
    
    // call the superclass' draw routine
    myTransferRGBBox::draw();

    computeRGBEndPoints(x0, y0, x1, y1); // this fills the x?,y?

    // find out which channels should be drawn
    transferRGBEditorPanel->affectChannels(drawChannel[0], drawChannel[1], drawChannel[2]);

    fl_clip(x(),y(),w(),h());

    for (int j = 0 ; j < 3 ; ++j) {
	if (!drawChannel[j])
	    continue; // skip that channel
	switch(j) {
	case 0:
	    fl_color(FL_RED);
	    break;
	case 1:
	    fl_color(FL_GREEN);
	    break;
	case 2:
	    fl_color(FL_BLUE);
	    break;
	}

	for (int i = 0 ; i < w() ; i++) {
	    xx = (int)((double)i * 256 / w());
	    fl_line(x()+i, y()+h()-1, x()+i, y()+h()-(int)((double)theRGBHisto[xx][j]*h()/maxval));
	}
    }

    fl_pop_clip();
    return;
}

