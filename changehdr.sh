#! /bin/sh
	
mkdir /tmp/imview
mkdir /tmp/imview/io
mkdir /tmp/imview/server

for file in *.h *.c *.cxx *.hxx io/*.cxx io/*.hxx server/*.cxx server/*.hxx ; do
    tmpfile=/tmp/imview/$file
    sed -n -e '/^.*/p' -e '/^ \* \*\//q' $file > /tmp/imview/oldheader
    nblines=`wc -l /tmp/imview/oldheader | awk '{print $1}'`
    sed -e "1,$nblines d" $file > $tmpfile
    cat header.txt $tmpfile > $file
done

rm -rf /tmp/imview
