/*
 * $Id: machine.hxx,v 4.5 2009/05/07 22:31:35 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 *
 * Include file for non-portable bits of imview
 *
 * Hugues Talbot	20 Jan 2000
 *      
 *-----------------------------------------------------------------------*/

#ifndef MACHINE_H
#define MACHINE_H

#include <stdio.h>
#include <setjmp.h>
#include <signal.h>
#include <imcfg.h>

#if HAVE_PTHREADS
#include <pthread.h>
#endif

#include "imview.hxx"

#ifdef WIN32_NOTCYGWIN

  typedef jmp_buf im_jmp_buf;

#  define  im_longjmp(X,Y)  longjmp((X),(Y))
#  define  im_setjmp(X,Y)  setjmp((X))
#  define  im_signal(X,Y)  {/* empty statement */} // for when a signal is not supported 
 
  typedef unsigned char  uchar;
  typedef unsigned short ushort;
  typedef unsigned int   uint;
  typedef unsigned long  ulong;
#  ifdef _MSC_VER
#    define im_va_start(X,Y) va_start((char *)(X),(Y))
#else // only vc++ has a problem?
#    define im_va_start va_start
#endif
#  define random rand  // not as good but will do.

#else // WIN32_NOTCYGWIN

  typedef sigjmp_buf im_jmp_buf;
# define  im_longjmp(X,Y) siglongjmp((X),(Y))
# define  im_setjmp(X,Y)  sigsetjmp((X),(Y))
# define  im_signal(X,Y)  signal((X),(Y))

#  ifndef O_BINARY
#    define O_BINARY 0
#  endif

#  define im_va_start va_start
#endif // WIN32_NOTCYGWIN

// DEC threads
#if HAVE_PTHREADS
#  if HAVE_DECTHREADS
#    define pthread_attr_init pthread_attr_create
#  else // HAVE_DECTHREAD
   extern pthread_mutexattr_t *pthread_mutexattr_default;
   extern pthread_condattr_t *pthread_condattr_default;
#  endif
extern pthread_t noThread; // a global
#else // HAVE_PTHREADS
// fake 'em if we don't have 'em
  typedef int Semaphore;
  void semaphore_down(int *);
  void semaphore_up(int *);
  void semaphore_init(int *);
  void semaphore_destroy(int *);
#endif // HAVE_PTHREADS

// prototypes
void imtempnam(char *name);
char const *systemName(void);
const char *myBaseName(const char *astring);
const char *myDirName(const char *astring);
int myFork(void);
void synchronize_GUI(void);
int skipzero(FILE *fp);
int myusleep(unsigned int usec);
endianness checkendian(void);


// bizarre stuff needed by MSVC++
#ifdef _MSC_VER
#define popen _popen    // How does Microsoft get away with things like that?
#define pclose _pclose
#endif // _MSC_VER

#endif // MACHINE_H
