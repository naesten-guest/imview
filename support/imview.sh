#!/bin/sh
#
# Imview fake shell script


# This shell script will be overwritten by the installation script
echo "If you get this message, imview's installation somehow failed"
echo "Contact the author: Hugues.Talbot@csiro.au"
echo "Sorry about that"

exit 1
