/*
 * $Id: toolbar.hxx,v 4.3 2004/06/23 15:58:17 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Most of imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */


/*------------------------------------------------------------------------
 *
 * Simple toolbar, originally. Is sure to become complicated as hell...
 *
 * Hugues Talbot	29 Jan 2001
 *      
 *-----------------------------------------------------------------------*/

#ifndef TOOLBAR_H
#define TOOLBAR_H

#include "imview.hxx"
#include "imnmspc.hxx" // namespace def. if required

#include <FL/Fl.H>
#include <FL/x.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Menu_Button.H>
#include <FL/Fl_Choice.H>
#include <FL/fl_draw.H>


class tipButton: public Fl_Button {
public:
    tipButton(int x, int y, int w, int h, const char *label=0);
    ~tipButton();
    int handle(int event); // so we can handle ENTER and LEAVE events
    void chg_cursor(Fl_Cursor c);
    void message(const char *m) {message_ = m;}
    void parentToolbar(class toolbar *t) {parentToolbar_ = t;}
private:
    const char *message_;
    class toolbar *parentToolbar_;
};

class toolbar {
public:
    toolbar();
    ~toolbar();
    void               setDefaults();
    void               show() {toolbarWindow->show();}
    void               hide() {toolbarWindow->hide();}
    friend Fl_Window  *toolbar_panel(toolbar &tb);

private:
    void               setMessage(const char *m) {}
    Fl_Window         *toolbarWindow;
};

#endif // TOOLBAR_H
