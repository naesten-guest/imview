/*
 * $Id: transferHistoBox.cxx,v 4.0 2003/04/28 14:40:14 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * A subclass of Box for drawing transfer functions
 *
 * Hugues Talbot	 3 May 1998
 *      
 *-----------------------------------------------------------------------*/

#include <math.h>

#include "imview.hxx"
#include "imageIO.hxx"
#include "imageViewer.hxx"
#include "transferFunction.hxx"
#include "transferBox.hxx"
#include "transferHistoBox.hxx"

extern imageIO *IOBlackBox;
extern imageViewer *mainViewer;
extern transfer  *transferEditorPanel;

void myTransferHistoBox::makeHisto()
{
    int i, spp, val;
    long nbpix;
    unsigned char *p, *end;
    
    for (i = 0 ; i < 256 ; i++)
	theHisto[i] = 0;

    nbpix = IOBlackBox->imageWidth() * IOBlackBox->imageHeight();
    spp = IOBlackBox->imageDepth();
    
    if (spp == 1) {
	p = IOBlackBox->imageData();
	end = p + nbpix;

	while (p != end) {
	    theHisto[*p++]++; // simple.
	}
    } else if (spp == 3) {
	// we need to be a bit more conservative with colour
	p = IOBlackBox->imageData();
	end = p + spp * nbpix;

	while (p != end) {
	    val = *p++;
	    val += *p++;
	    val += *p++;

	    theHisto[val/3]++; // simple as well.
	}
	
    }

    maxval = 0;
    for (i = 0 ; i < 256 ; i++)
	if (theHisto[i] > maxval)
	    maxval= theHisto[i];
	
    return;
}

void myTransferHistoBox::draw()
{
    double x0, y0, x1, y1;
    int xx;
    
    // call the superclass' draw routine
    myTransferBox::draw();

    x0 = y0 = x1 = y1 = 0.0;
    computeEndPoints(x0, y0, x1, y1); // this fills the x?,y?
    
    fl_clip(x(),y(),w(),h());
    fl_color(FL_BLACK);

    for (int i = 0 ; i < w() ; i++) {
	xx = (int)((double)i * 256 / w());
	fl_line(x()+i, y()+h()-1, x()+i, y()+h()-(int)((double)theHisto[xx]*h()/maxval));
    }


    fl_pop_clip();
    return;
}

