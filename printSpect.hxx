/*
 * $Id: printSpect.hxx,v 4.0 2003/04/28 14:39:57 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * A printPreferenceWindow class for spectra
 *
 * not Experimenting OOP with fluid anymore!
 *
 * Hugues Talbot	 2 Sep 1998     
 *-----------------------------------------------------------------------*/

#ifndef PRINTSPECT_H
#define PRINTSPECT_H

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Return_Button.H>
#include <FL/Fl_Window.H>
#include "imview.hxx"
#include "io/gplot2.hxx"

typedef enum {
    A4_PAGE = 0,
    LETTER_PAGE = 1,
    A3_PAGE = 2
} pagetype;

typedef enum {
    LANDSCAPE = 0,
    PORTRAIT = 1
} pageorient;

typedef enum {
    LINES = 0,
    SPLINES = 1
} jointype;

class printspect {
public:
    printspect();
    void setDefaults();
    void toggleToFile();
    void toggleToPrinter();
    void allowTitle(bool b);
    void printFileName(const char *fn);
    const char *filenameInputValue(void) { return filenameInput->value(); }
    void show();
    void hide();
    int doThePreview();
    int closePreview();
    int doThePrinting();
    void setCallerType(paneltype thisPanel) {whichPanel = thisPanel;}
    friend Fl_Window *printspect_panel(printspect &s);

private:
    paneltype             whichPanel;
    // the connection to gnuplot
    plot_t              *thePlot;
    // in the order they appear in fluid
    // -- The dialog window
    Fl_Window           *prefWindow; // the main window

    // Print type group
    Fl_Group            *printtypeGroup;
    Fl_Check_Button     *printerCheck, *fileCheck;

    // Input group
    Fl_Group            *inputGroup;
    Fl_Input            *commandInput;
    Fl_Input            *filenameInput;
    Fl_Button           *browseButton;
    Fl_Input            *titleInput;

    // Orientation Group
    Fl_Group            *orientationGroup;
    Fl_Check_Button     *portraitCheck, *landscapeCheck;

    // Paper size group
    Fl_Group            *papersizeGroup;
    Fl_Check_Button     *A4Check, *letterCheck, *A3Check;

    // Language Group
    Fl_Group            *joinGroup;
    Fl_Check_Button     *linesCheck, *splinesCheck;

    Fl_Input            *nbcopiesInput;

    
    Fl_Button           *previewButton, *CancelButton;
    Fl_Return_Button    *OKButton;
};



#endif // PRINTSPECT_H
