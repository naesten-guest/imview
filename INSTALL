# -*- text -*-
# $Id: INSTALL,v 4.6 2008/10/29 00:42:27 hut66au Exp $
#

This is Imview's installation documentation:
-------------------------------------------

These instruction relate to imview 1.1.x


Prerequisite
============

0-      This software works on Unix, MacOS X and Windows. No BeOS,
	no MacOS prior to OS/X, sorry (please help if you want a version 
	for your favourite O/S).

1-	You will need a reasonably ANSI/ISO compliant C++ compiler,

	The GNU Compiler Collection (GCC) version 3.1 or more recent
	is  such a compiler, that you can get from

		ftp://prep.ai.mit.edu/pub/gnu/

	and various mirrors. Earlier versions of GCC are no longer
	supported, sorry!

	The Intel C++ compiler, version 7.0 or later is also fine. On
	Linux this compiler is free for non-commercial use.

	On windows, MS Visual C++ version 6.0 patchlevel 3 is fine for
	the time being but plans are to move to a more recent version
	(probably version 7.1 a.k.a. .NET 2003).

	Other compilers might be fine also, YMMV. The configure 
	script will tell you if it thinks your compiler is not
	up to scratch (and the compilation will probably fail in
	that case).


2-	To build, imview *REQUIRES* FLTK, the Fast Light ToolKit.
	Get it from

		http://www.fltk.org

	Compile it and install it on your machine. As of this
	writing, the version I used was fltk-1.1.9.
es
	NOTE: The flags that I used to compile FLTK were:

	fltk% ./configure --enable-threads --enable-xft

	Neither of the two '--enable' switches are indispensable.

NOTE:   While this version of imview requires FLTK 1.1.x.
	FLTK-1.0.x is still supported in a different imview CVS branch,
	contact the author if you need that version.


FLTK works on Mac OS/X, most Unix distributions (including Linux) and
on Microsoft Windows (all versions >= win95) as well.

	Imview will not compile at all until FLTK is available
on your system. An increasing number of Linux distributions come
with FLTK available in their repository, but this is the case
for Debian, and therefore Ubuntu.

	FLTK itself requires the X11 windowing system OR the
windows win32 GDI that comes will all win32 distributions OR
the `carbon' environment that comes with MacOS X. All unix 
systems I know come with X11 (even MacOS X in fact!).

Recommended add-ons
===================

	While FLTK is the only true requirement, Imview will
take advantage of the following libraries/enhancements to the
O/S:

1- 	The TIFF library (originally by Sam Leffler): 

		http://www.libtiff.org/

	Any version after 3.4beta037 is fine. The current version
	is 3.8.x.

2-	The JPEG library

		ftp://ftp.uu.net/graphics/jpeg/

	I used version v6b.

3- 	The ImageMagick library. This will add dozens of
	formats to those that Imview knows of natively, at the
	cost of a relatively big library (which can be shared).
	While ImageMagick is somewhat unstable I still recommend
	you use it. Version 5.x or 6.x is necessary.

		http://www.simplesystems.org/ImageMagick/

	In turn, ImageMagick may require:

	3a- 	the PNG library

		ftp://ftp.uu.net/graphics/png/

		Current version is 1.0.8

	3b-     the ZLIB library

		ftp://ftp.uu.net/pub/archiving/zip/zlib/

		Current version is 1.1.3

4-      POSIX Threads. Most recent Unix systems come with POSIX-compliant
	threads, there is a pthread library for win32 available at:

		ftp://sources.redhat.com/pub/pthreads-win32/

	You don't need to bother with the source, just download the DLLs.
	Imview can compile without threads but you would be giving up
        on the image server, which is a major feature of imview.


Without the image libraries, Imview will be unable to load the
corresponding image formats. Theoretically ImageMagick should be
enough (it can also read jpeg and tiff images) but its TIFF support is
not great. Imview can read just about any TIFF file, not just the
common 8 bit-per-channel ones. The native JPEG support is there for
historical reasons.

Without the POSIX thread library, the Imview server will not run (see the
Imview documentation).

Most Linux distributions come with all the libraries listed above (except
FLTK), one notable exception being the (now defunct) Corel distro. 

Building Imview
===============

1 Under Unix:
-------------

To build imview, normally you only need to do:

 % ./configure
 % make
 % make install


1.1 Options
-----------

1.1.1 Standard options

	As is standard with every ./configure scripts, lots of
options are available. Try ./configure --help for a list of them!
If you are not familiar with autoconf-generated ./configure, just
about the only really useful one is --prefix, which specifies
where to put your software and where to look for libraries. Normally
you would put everything under the one hierarchy, by default under
/usr/local

	
1.1.2 Imview-specific options

  --enable-prof           enable prof source profiling support (default is no)
  --enable-gprof          enable gprof source profiling support (default is no)
  --enable-gcov           enable gcov source profiling support (default is no)
  --without-jpeg          disable JPEG support
  --without-tiff          disable TIFF support
  --without-magick        disable ImageMagick support
  --enable-debug          turn on debugging [default=no]
  --without-server        disable image server [default=no]
  --without-sysv          disable Unix SYSV shared memory support [default=no]
  --with-x                use the X Window System

	Unless you know what you are doing, you shouldn't need to mess with
these, except maybe --enable-debug, if you've found a bug in Imview that you
want to track (by default, ./configure will set things up to  build a production 
release, optimized without symbols).

1.1.3 A neat trick

	If you are going to build imview for a number of platform, I suggest
that you create a obj/<platform>  directory, that you cd to this directory
and that you run ../../configure from there. Then build imview the normal way
in this directory (just run make). This works VERY well (I use it all the time). 
	The big benefit is of course that you can have as many as these
<platform> subdirectories as you want and that you don't need to clean and
re-configure between them (if you've got the hard disk space, that is).

2 For windows:
----------------

There are at least three routes to build Imview for win32:

2.1: Use a cross-compiler
-------------------------

	It is possible to compile Imview with a cross-compiler, this is supported.
	I cross-compile imview for win32 on both Linux and OS/X. To do that,
you need a mingw32 cross compiler, and at configure time, simply specify 

 % [./]configure --host=mingw32 <other options>

2.2: Use GCC under windows
--------------------------

There are at least two ways of running GCC under windows:

2.2.1: the mingw32 environment
------------------------------

	Mingw32 stands for "minimal" GNU for win32. It uses the GNU tools
together with Microsoft's implementation of the ANSI/ISO C and POSIX libraries to 
make things work. The benefits are that executables are relatively small and 
portable, and no extra library is necessary besides those that ship with Windows.

	Point your browser to http://www.mingw.org to download the compiler and
the environment. The development tools are in the large MinGW.exe package, and
the minimal compiling enviroment (including shell) is called MSYS. This MSYS
environment is almost like a normal Unix environment, except it's Windows. To

	Nowadays MinGW comes with pre-compiled versions of libtiff, libjpeg,
libpng and libz, You'll only need to compile FLTK yourself. Also you'll need 
POSIX threads to be able to run the server, this can be provided by pthread-win32:

	http://sources.redhat.com/pthreads-win32/

	I've yet to succeed in compiling ImageMagick from within mingw, but
I'm sure it's feasible, but apart from that the rest of the libraries that
imview uses all compile and work fine.

	To configure and compile Imview, just proceed as under Unix above.

2.2.2: the Cygwin environment
-----------------------------
	
	Cygwin is an effort to port the whole Unix environment to Windows: X11,
shells, utilities, libraries, and development tools:

	http://www.cygwin.com

	From within the cygwin framework you can install pre-compiled version
of everything Imview uses: FLTK, libjpeg, libtiff, etc.

	You can ./configure the cygwin environment exactly as with any other
Unix platform, the script will detect your environment and compile imview 
correctly. It will not require X11 to run, but it will require the Cygwin
libraries, which in my opinion makes it an inferior solution to the mingw32
route for portability, but might make sense in your setup.

	Cygwin provide their own posix thread library, so you don't need the
pthread-win32 library that you need if you compile with mingw. Also the 
ImageMagick library that ships with Cygwin requires X11, so at the moment
it's not compatible with imview.

	At any rate the end result works OK but there are some unresolved issues, 
see the TODO file.


2.3: Use MSVC++
---------------

	The two solutions above work fine for 99% of cases, but I've discovered 
a bug in  the pthread-win32 library that is such that version of Imview compiled with
it under any version of GCC for windows will not run well on Windows NT-4.0 on true
SMP machines (i.e: If you are running windows NT on a dual-CPU machine or more).
This has to do with exception handling. The problem has been reported and might
have been fixed but I haven't tested it... It might have gone away with win2k or XP 
but I seriously doubt it.

	To make Imview work well with Pthreads on multiple CPUs under NT, you need
to compile with MSVC++. A project file is included in the  obj/NT-msvc++ directory in
this distribution. You will need to edit it (by hand!) to indicate the location
of the various libraries (fltk, etc). I used MSVC++ version 6.0 patchlevel 3.

	Due to poor template support in MSVC++ 6.0, some functionality is reduced, 
but this should hardly be noticeable to the casual user.

BTW, I find MSVC++ about 5 times slower (to compile) than the cross-compiler solution.
The end result works fine, of course.
	

3 For Mac.
---------

Imview works very well on MacOS/X. Last tested on 10.4.11, no problem reported.
Display uses Aqua/Carbon, not X11, thanks to FLTK support. Support for Cocoa is
foreseen for when FLTK-2.0 comes out of alpha.

4 Known issues
================

Known platforms;

	Imview is known to work on the following platforms

Linux glibc-2.x: 
	Redhat 5.x, 6.x, 7.x, 8.x and 9, 
	Fedora FC1-FC7
	Debian 2.2, 3.0 and following, including Ubuntu up to 8.04

DEC Unix (Compaq Tru64) 3.x, 4.x, 5.x
Solaris 2.5.1, Solaris 7, Solaris 8, Solaris 9
MS-Windows win32: win9x, win-NT 3.51, win-NT 4.0, win2k, winXP
MacOS: Mac OS/X 10.0.3, 10.3.x, 10.4.x

	I welcome any news on any other platform.

Known bugs:
-----------

	There are a few known bugs. Please see the full documentation.


Compilation and installation issues:
-----------------------------------

Here are a few non-platform-dependent issues:

 Warnings:

	There should be very few warnings when you compile imview,
	except on Solaris (see below). Depending on your compiler,
	you will probably see a few in imtranslate.cxx. They relate
	to template instanciation and can be ignored.

Here are a few platform-dependent issues.

  Solaris: The X11 headers are stuffed, Sun does not provide a 
	set of ANSI-compliant X11 headers: the prototypes are 
	incomplete. Gcc prior to 2.95 just didn't care but 
	suddently from 2.95 it refuses to compile
	these headers unless the -fpermissive flag is added. The
	configure script checks for that *BUT* you will most
	likely get a *TON* of warnings. gcc-2.95.3 has a workaround
	that works if the set of headers that you use come from
	/usr/include/X11. Myself I've just patched gcc so this
	problem disappears. Contact me if you want to get it, it's
	a one-line change.

	You may have to specify --x-libraries=/usr/openwin/lib to
	the configure script if you get an error with the
	exception structure being multiply defined under Solaris-2.5.1



FINALLY:
========


Thank you for your interest in Imview. Let me know how you go!

First version:
Hugues Talbot	22 Dec 2000

Last edited:
Hugues Talbot	 27 Oct 2008	-- Release 1.1.9d
