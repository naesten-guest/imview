/*
 * $Id: transferFunction.hxx,v 4.1 2003/09/09 07:46:04 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * The long-awaited contrast/brightness/gamma panel
 *
 * Hugues Talbot	 3 May 1998
 *
 *      
 *-----------------------------------------------------------------------*/

#ifndef TRANSFERFUNCTION_H
#define TRANSFERFUNCTION_H

#include <FL/Fl.H>
#include <FL/x.H>
#include <FL/Fl_Slider.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Float_Input.H>
#include <FL/Fl_Int_Input.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Return_Button.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Double_Window.H>
#include "transferBox.hxx"
#include "transferHistoBox.hxx"
#include "transferFunctionBox.hxx"

enum {
    TRANSFER_DSP = 0,
    HISTOGRAM_DSP = 1
};

void chosendisplay_cb(Fl_Menu_ *, void *);

    
class transfer {
public:
    transfer();
    ~transfer();
    void setDefaults();
    void computeDrawingParms();
    void setDrawingParms();
    void redrawWindowIfNeeded();
    void show();
    void hide();
    void setGammaSliderValue(float val) {gammaSlider->value(val);}
    void setGammaInputValue(const char *c) {gammaInput->value(c);}
    void setContrastSliderValue(float val) {contrastSlider->value(val);}
    void setBrightnessSliderValue(float val) {brightnessSlider->value(val);}
    void setTopValue(int t);
    void setBottomValue(int t);
    void setLeftValue(int t);
    void setRightValue(int t);
    void setTransfer();
    void setHisto();
    void getParameters(float &theGamma, float &theContrast, float &theBrightness) {
	transferBox->getParms(theContrast, theBrightness, theGamma);
    }
    void ensureImageRedraw(bool s) {transferBox->setRelease(s);}
    friend Fl_Double_Window *transfer_panel(transfer &s);

private:
                        
    Fl_Double_Window        *transferWindow;
    Fl_Choice               *displayChoice;
    Fl_Float_Input          *gammaInput;
    Fl_Int_Input            *topInput, *bottomInput, *leftInput, *rightInput;
    myTransferBox           *transferBox, *memoryBox;
    myTransferHistoBox      *transferHistoBox;
    myTransferFunctionBox   *transferFunctionBox;
    Fl_Slider               *contrastSlider, *brightnessSlider;
    Fl_Slider               *gammaSlider;
    Fl_Button               *resetButton, *normaliseButton;
    Fl_Check_Button         *applyToAllButton;
    Fl_Return_Button        *OKButton;
};

#endif // TRANSFERFUNCTION_H
