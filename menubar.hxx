/*
 * $Id: menubar.hxx,v 4.13 2009/05/07 22:31:35 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * The application main menu
 *
 * Hugues Talbot	16 Nov 1997
 *
 *-----------------------------------------------------------------------*/

#ifndef MENUBAR_H
#define MENUBAR_H

#ifndef Fl_Object_H
#include <FL/Fl_Object.H>
#endif


#include <FL/Fl.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Sys_Menu_Bar.H>


#define IMAGE_LIST_ID      0
#define CLUT_LIST_ID       1
#define LAST_LIST_ID       2

#define ONLINEHELP         "quickhelp.html"
#define LICENSE            "gpl.html"
#define WARRANTY           "warranty.html"
// #define HELPURL            "http://extra.cmis.csiro.au/IA/talbot/ee02only/imview/"
#define HELPURL            "http://www.cmis.csiro.au/Hugues.Talbot/imview/online/imview.html"
#define LOCALHELP          "/usr/share/doc/imview-doc/html/imview.html"

#define IMAGE_LIST_BEGIN   "Image list"
#define CLUT_LIST_BEGIN    "Default Colourmap"

// menu items that have variable flags, for which
// their name is important
#define NEXTIMAGE_ITEM           "Next image"
#define PREVIOUSIMAGE_ITEM       "Previous image"
#define OPEN_AS_RAW              "Open raw"
#define OVERLAY_SUBMENU          "Overlay"
#define OPEN_OVERLAY_ITEM        "Open overlay"
#define CLOSE_OVERLAY_ITEM       "Close overlay"
#define CLOSE_ITEM               "Close"
#define SAVE_ITEM                "Save"
#define PRINTPREFS_ITEM          "Print"
#define WRAPLUT_ITEM             "Wraparound LUT"
#define SYNCDISP_ITEM            "Synchronize display"
#define COLOURMAP_ITEM           "Colourmap"
#define VIEW3D_ITEM              "View 3D"
#define ROTATE3DMENU_ITEM        "3D rotate"
#define NEXTCOMPONENT_ITEM       "Next sample"
#define PREVIOUSCOMPONENT_ITEM   "Previous sample"
#define SELECTCOMPONENT_ITEM     "Select sample"
#define HYPERSPECTRAL_ITEM       "Show spectrum"
#define DEPTHPROFILE_ITEM        "Show depth profile"
#define RGBTRANSFER_ITEM         "RGB contrast/brightness"
#define BIVHIST_ITEM             "Bivariate histogram"
#define NEXTPLANE_ITEM           "Next plane"
#define PREVIOUSPLANE_ITEM       "Previous plane"
#define SELECTPLANE_ITEM         "Select plane"
#define NEXTFRAME_ITEM           "Next frame"
#define PREVIOUSFRAME_ITEM       "Previous frame"
#define SELECTFRAME_ITEM         "Select frame"
#define WIN_FIT_IMG_ITEM         "Fit &window to image"
#define IMG_FIT_WIN_ITEM         "Fit &image to window"
#define DECOUPLE_IMG_WIN_ITEM    "&De-coupled image and window"


// preference definition
enum imview_quickpref {
    PREF_SMOOTHUNZOOM = 0,  // smooth when unzooming
    PREF_DEBUG,             // debug on/off
    PREF_KEEPPOINTS,        // keep point when switching images (or not)
    PREF_STOPDEBUG,         // stop after each debug message (not recommended!)
    PREF_WARNING,           // show warnings
    PREF_WRAPAROUND,        // make the LUT wrap around after 255 rather than fit to min/max
    PREF_MAKECHAR,          // make images CHAR if no data is lost that way (the non-char data fits into chars)
    PREF_RGBRGB,            // RGB images are displayed as RGB, not as multi-spectral images
    PREF_FITALL,            // fit grey-levels/colours over all samples, not just the one displayed
    PREF_FIT3D,             // fit grey-level/colours over all slices
    PREF_SYNCDISP,          // synchronize display with the imview server
    PREF_HIDEMENU,          // totally hide the main menu
    PREF_FULLSCREEN,        // fullscreen mode
    LAST_INT_ARG,           // END marker.
};

#define MODE_POINTFILE        0

// apparently all the callbacks have to be global functions
void openfile_cb(const char *s);
void test_cb(Fl_Object *,void *i);
void open_cb(Fl_Object *, void *);
void openraw_cb(Fl_Object *, void *);
void openoverlay_cb(Fl_Object *, void *);
void closeoverlay_cb(Fl_Object *, void *);
void save_cb(Fl_Object *, void *);
void printprefs_cb(Fl_Object *, void *);
void close_cb(Fl_Object *, void *);
// help
void whodunit_cb(Fl_Object *, void *);
void onlinehelp_cb(Fl_Object *, void *);
void fulldocument_cb(Fl_Object *, void *);
void licence_cb(Fl_Object *, void *);
void warranty_cb(Fl_Object *, void *);
// histogram
void transfer_cb(Fl_Object *, void *);
void transferRGB_cb(Fl_Object *, void *);
void threshold_cb(Fl_Object *, void *);
// zoom
void zoomin_cb(Fl_Object *,void *);
void zoomout_cb(Fl_Object *,void *);
void smallzoomin_cb(Fl_Object *,void *);
void smallzoomout_cb(Fl_Object *,void *);
void unzoom_cb(Fl_Object *,void *);
void selectzoom_cb(Fl_Object *, void *);
void thiszoom_cb(Fl_Object *, void *);
void selectdefaultzoom_cb(Fl_Object *, void *);
// 3D viewer
void showview3d_cb(Fl_Object *, void *);
// rotations & flips
void rotate90right_cb(Fl_Object *, void *);
void rotate90left_cb(Fl_Object *, void *);
void rotate180_cb(Fl_Object *, void *);
void rotate3dup_cb(Fl_Object *, void *);
void rotate3ddown_cb(Fl_Object *, void *);
void flipv_cb(Fl_Object *, void *);
void fliph_cb(Fl_Object *, void *);
// image list manipulation
void image_cb(Fl_Object *, void *);
void nextimage_cb(Fl_Object *, void *arg);
void previousimage_cb(Fl_Object *, void *arg);
void saveimagelist_cb(Fl_Object *, void *);
// various manipulation on images
void nextcomponent_cb(Fl_Object *, void *);
void previouscomponent_cb(Fl_Object *, void *);
void showprofile_cb(Fl_Object *, void *);
void showspectrum_cb(Fl_Object *, void *);
void showbivhist_cb(Fl_Object *, void *);
void setcomponentvalue(int v);
void selectcomponent_cb(Fl_Object *, void *);
void showdepthprofile_cb(Fl_Object *, void *);
void nextplane_cb(Fl_Object *, void *);
void previousplane_cb(Fl_Object *, void *);
void setplanevalue(int v);
void selectplane_cb(Fl_Object *, void *);
void setframevalue(int v);
void nextframe_cb(Fl_Object *, void *);
void previousframe_cb(Fl_Object *, void *);
void selectframe_cb(Fl_Object *, void *);
void imageinfo_cb(Fl_Object *, void *);
void toolbar_cb(Fl_Object *, void *);
int setClut(const char *label); // Hmmm
void clut_cb(Fl_Object *,void *);
void noclut_cb(Fl_Object *,void *);
void mode_cb(Fl_Object *,void *);
// Charlotte
void showregisterpanel_cb(Fl_Object *, void *);
// user preferences
void userprefspanel_cb(Fl_Object *, void *);
void preference_cb(Fl_Object *,void *);
void displaymode_cb(Fl_Object *,void *);
void fullscreen_cb(Fl_Object *,void *);
void hidemainmenu_cb(Fl_Object *, void *arg);
void showmainmenu_cb(Fl_Object *, void *arg);
// pointfiles
void openpointfile_cb(Fl_Object *,void *);
void newpointfile_cb(Fl_Object *, void *);
void closepointfile_cb(Fl_Object *,void *);
void deleteallpoints_cb(Fl_Object *,void *);
void deletelastpoint_cb(Fl_Object *,void *);
void setxorvalue_cb(Fl_Object *,void *);
// server
void startserver_cb(Fl_Object *, void *);
void stopserver_cb(Fl_Object *, void *);
void showprogress_cb(Fl_Object *, void *);
// graceful exit
void quit_cb(Fl_Object *,void *);
void noquit_cb(Fl_Object *,void *);

// a global wrapper
int openimage(const char *imname, int frame=-1);

class imViewMenuBar: public Fl_Menu_Bar {
public:
    imViewMenuBar(int x, int y, int w, int h, const char *label = 0);
    ~imViewMenuBar();
    int addToItemList(const char *newImage, int listID);
    void removeFromItemList(const char *oldImage, int listID);
    void *getSelectedItemArgument(int listID);
    Fl_Menu_Item *getSelectedMenuItem(int listID);
    void *getItemByLabel(const char *givenLbl, int listID);
    char *getItemListContent(int listID, int &nbitems);
    void *getNextItemInList(int listID);
    void *getPreviousItemInList(int listID);
    int isAlreadyInItemList(const char *newImageName, int listID);
    void setFlagsByName(const char *itemName, int newFlags, Fl_Menu_Item *aMenu = 0, int menuSize = 0);
    int getFlagsByName(const char *itemName, Fl_Menu_Item *aMenu = 0, int menuSize = 0);
    int getNbImagesInList(void) { return nbImagesInList; }
    void setSysMenu(void);
    void draw();
private:
    Fl_Menu_Item *dynamicMenu, *staticMenu_;
    Fl_Sys_Menu_Bar *sysmenu_;
    // global variables for the menu application
    const char *listName[LAST_LIST_ID];
    void *listCallback[LAST_LIST_ID]; // this will be a list of functions
    int   nbImagesInList;    
};


#endif // MENUBAR_H
