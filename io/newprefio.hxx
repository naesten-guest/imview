/*
 * $Id: newprefio.hxx,v 1.5 2004/06/21 16:12:51 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * New preference I/O mechanism, using the FLTK facility this time,
 * unfortunately not STL-based.
 *
 * Hugues Talbot	 2 Jun 2003
 *
 *
 *
 *-----------------------------------------------------------------------*/


#ifndef NEWPREFIO_H
#define NEWPREFIO_H

#include <string.h>
#include <FL/Fl.H>
#include <FL/Fl_Preferences.H>

typedef enum {PSREND_COLOUR=0,PSREND_GREY, PSREND_BW} psrendertype;
typedef enum {ZOOM_UNCONSTRAINED = 0, ZOOM_CONSTRAINED} zoomtype;

// the imview preferences
class imprefs {
public:
    imprefs();
    ~imprefs();

    // setters
    // data in (from the dialog)
    void prefspath(const char *p) {strncpy(prefsPath_, p, DFLTSTRLEN-1);}
    void smoothZoomout(bool s) {smoothZoomout_ = s;}
    void keepPoints(bool s) { keepPoints_ = s;}
    void mouseZoomType(zoomtype z) {mouseZoomType_ = z;}
    void overlayTransparency(double t) {overlayTransparency_ = t;}
    void gspath(const char *s) {strncpy(gspath_, s,DFLTSTRLEN-1);}
    void psRenderRes(double r) {psRenderRes_ = r;}
    void psDisplayRes(double r) {psDisplayRes_ = r;}
    void psBoundingBox(page_size b) {psBoundingBox_ = b;}
    void gvpath(const char *s) {strncpy(gvpath_, s,DFLTSTRLEN-1);}
    void psRenderDepth(psrendertype p) {psRenderDepth_ = p;}
    void psRenderSmooth(bool s) {psRenderSmooth_ = s;}
    void psAntialiasRender(bool s) {psAntialiasRender_ =s;}
    void pollFrequency(double f) {pollFrequency_ = f;}
    void requirePassword(bool p) {requirePassword_ = p;}
    void localhostOnly(bool l) {locahostOnly_ = l;}
    void hideMenu(bool h) {hideMenu_ = h;}
    void xorBreakLines(const char *s) {strncpy(xorBreakLines_, s, SMALSTRLEN-1);}
    void xorProfileLines(const char *s) {strncpy(xorProfileLines_, s, SMALSTRLEN-1);}

    // getters
    // to the user
    const char      *prefspath(void) {return prefsPath_;}
    bool             smoothZoomout(void) {return smoothZoomout_;}
    bool             keepPoints(void) {return keepPoints_;}
    zoomtype         mouseZoomType(void) {return mouseZoomType_;}
    double           overlayTransparency(void) {return overlayTransparency_;}
    const char      *gspath(void) {return gspath_;}
    const char      *gvpath(void) {return gvpath_;}
    double           psRenderRes(void) {return psRenderRes_;}
    double           psDisplayRes(void) {return psDisplayRes_;}
    psrendertype     psRenderDepth(void) {return psRenderDepth_;}
    page_size        psBoundingBox(void) {return psBoundingBox_;}
    bool             psRenderSmooth(void) {return psRenderSmooth_;}
    bool             psAntialiasRender(void) {return psAntialiasRender_;}
    const char      *xorBreakLines(void) {return xorBreakLines_;}
    const char      *xorProfileLines(void) {return xorProfileLines_;}
    bool             requirePassword(void) {return requirePassword_;}
    bool             localhostOnly(void) {return locahostOnly_;}
    bool             hideMenu(void) {return hideMenu_;}
    double           pollFrequency(void) {return pollFrequency_;}

    
    void             defaultPrefs(void);
    int              readprefs(const char *fromfile);
    int              writeprefs(const char *tofile);

private:
    // one per tab
    Fl_Preferences   *imPrefs_;
    
    // general stuff
    char             *prefsPath_;
    bool              smoothZoomout_, keepPoints_;
    zoomtype          mouseZoomType_;
    double            overlayTransparency_;
    // Postscript
    char             *gspath_, *gvpath_;
    double            psRenderRes_, psDisplayRes_;
    psrendertype      psRenderDepth_;
    page_size         psBoundingBox_;
    bool              psRenderSmooth_, psAntialiasRender_;
    // server
    double            pollFrequency_;
    bool              requirePassword_, locahostOnly_;
    // expert stuff
    bool              hideMenu_;
    // debug stuff
    char             *xorBreakLines_, *xorProfileLines_;

};

#endif // NEWPREFIO_H
