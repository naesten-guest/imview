/*
 * $Id: newprefio.cxx,v 1.7 2007/06/23 13:47:17 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * New preference I/O mechanism, using the FLTK facilities.
 *
 * Hugues Talbot	 2 Jun 2003
 *
 *-----------------------------------------------------------------------*/

#include <string.h>
#include <assert.h>
#include <FL/Fl.H>
#include <FL/Fl_Preferences.H>
#include <FL/filename.H>
#include "imview.hxx"
#include "imnmspc.hxx" // namespace, etc
#include "newprefio.hxx"


imprefs::imprefs()
{
    dbgprintf("imprefs constructed\n");
    imPrefs_ = new Fl_Preferences(Fl_Preferences::USER,
				  "hugues.talbot_csiro.au",
				  "imview");

    // memory for char stuff
    prefsPath_ = new char[FL_PATH_MAX];
    gspath_    = new char[DFLTSTRLEN];
    gvpath_    = new char[DFLTSTRLEN];
    xorBreakLines_ = new char[SMALSTRLEN];
    xorProfileLines_ = new char[SMALSTRLEN];

    // set default values
    defaultPrefs();
}

imprefs::~imprefs()
{
    delete[] gspath_;
    delete[] gvpath_;
    delete[] xorBreakLines_;
    delete[] xorProfileLines_;

    delete imPrefs_;

    dbgprintf("imprefs destructed\n");
}

void imprefs::defaultPrefs()
{
    // general
    strcpy(prefsPath_, "");
    smoothZoomout_ = false; // performance
    keepPoints_ = false; // most logical?
    mouseZoomType_ = ZOOM_CONSTRAINED;
    overlayTransparency_ = 0.8;

    // Postscript
    strcpy(gspath_, "gs");
    strcpy(gvpath_, "ghostview");
    psRenderRes_ = 150.0; // speed
    psDisplayRes_ = 75.0; // looks good
    psRenderDepth_ = PSREND_COLOUR; // looks best
    psRenderSmooth_ = true; // looks over performance
    psAntialiasRender_ = true; // same here
    psBoundingBox_ = PAGESIZE_AUTO;

    // server
    pollFrequency_ = 100.0;
    requirePassword_ = false; // not implemented yet
    locahostOnly_ = false; // doesn't work anyway

    // expert stuff
#ifdef MACOSX_CARBON
    hideMenu_ = true; // on Macs we use the system menu.
#else
    hideMenu_ = false; // default not an expert
#endif
     
    // debug stuff
    strcpy(xorBreakLines_,"0xffffeffe");
    strcpy(xorProfileLines_, "0x1b2dc3a4"); // esoteric
}

// pretty obvious
int imprefs::readprefs(const char *)
{
    int  ival;
    assert(imPrefs_ != 0);

    imPrefs_->getUserdataPath(prefsPath_, FL_PATH_MAX);
    dbgprintf("Data path = (%s)\n", prefsPath_);
    
    Fl_Preferences imGeneral(*imPrefs_, "General");
    imGeneral.get("smoothZoomOut", ival, 0);
    smoothZoomout_ = (ival > 0);
    imGeneral.get("keepPoints", ival, 0);
    keepPoints_ = (ival > 0);
    imGeneral.get("mouseZoomType", ival, 0);
    mouseZoomType_ = (zoomtype)ival;
    imGeneral.get("overlayTransparency", overlayTransparency_, 0.8);

    Fl_Preferences imPostscript(*imPrefs_, "Postscript");
    imPostscript.get("gspath", gspath_, "gs", DFLTSTRLEN-1);
    imPostscript.get("gvpath", gvpath_, "ghostview", DFLTSTRLEN-1);
    imPostscript.get("psRenderRes", psRenderRes_, 150.0);
    imPostscript.get("psDiplayRes", psDisplayRes_, 75.0);
    imPostscript.get("psRenderDepth", ival, 0);
    psRenderDepth_ = (psrendertype)ival;
    imPostscript.get("psRenderSmooth", ival, 0);
    psRenderSmooth_ = (ival>0);
    imPostscript.get("psAntialiasRender", ival, 0);
    psAntialiasRender_ = (ival > 0);
    int psbb = psBoundingBox_;
    imPostscript.get("psBoundingBox", psbb, 0);
    
    Fl_Preferences imServer(*imPrefs_, "Server");
    imServer.get("pollFrequency", pollFrequency_, 100.0);
    imServer.get("requirePassword", ival, 0);
    requirePassword_ = (ival>0);
    imServer.get("locahostOnly", ival, 0);
    locahostOnly_ = (ival > 0);
    
    Fl_Preferences imExpert(*imPrefs_, "Expert");
    imExpert.get("hideMenu", ival, 0);
    hideMenu_ = (ival > 0);

    Fl_Preferences imDebug(*imPrefs_, "Debug");
    imDebug.get("xorBreakLines", xorBreakLines_, "0xffffeffe", SMALSTRLEN-1);
    imDebug.get("xorProfileLines", xorProfileLines_, "0x1b2dc3a4", SMALSTRLEN-1);

    return 0;
}

// even more obvious
int imprefs::writeprefs(const char *)
{
    assert(imPrefs_ != 0);
    
    Fl_Preferences imGeneral(*imPrefs_, "General");
    imGeneral.set("smoothZoomOut", (int)smoothZoomout_);
    imGeneral.set("keepPoints", (int)keepPoints_);
    imGeneral.set("mouseZoomType", (int)mouseZoomType_);
    imGeneral.set("overlayTransparency", overlayTransparency_);

    Fl_Preferences imPostscript(*imPrefs_, "Postscript");
    imPostscript.set("gspath", gspath_);
    imPostscript.set("gvpath", gvpath_);
    imPostscript.set("psRenderRes", psRenderRes_);
    imPostscript.set("psDiplayRes", psDisplayRes_);
    imPostscript.set("psRenderDepth", (int)psRenderDepth_);
    imPostscript.set("psRenderSmooth", (int)psRenderSmooth_);
    imPostscript.set("psAntialiasRender", (int)psAntialiasRender_);
    imPostscript.set("psBoundingBox", (int)psBoundingBox_);
    
    Fl_Preferences imServer(*imPrefs_, "Server");
    imServer.set("pollFrequency", pollFrequency_);
    imServer.set("requirePassword", (int)requirePassword_);
    imServer.set("locahostOnly", (int)locahostOnly_);
    
    Fl_Preferences imExpert(*imPrefs_, "Expert");
    imExpert.set("hideMenu", (int)hideMenu_);

    Fl_Preferences imDebug(*imPrefs_, "Debug");
    imDebug.set("xorBreakLines", xorBreakLines_);
    imDebug.set("xorProfileLines", xorProfileLines_);
    
    imPrefs_->flush();
    dbgprintf("Preferences flushed\n");

    return 0;
}
