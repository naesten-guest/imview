/*
 * $Id: gplot2.hxx,v 4.2 2008/10/27 15:34:12 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */


/*------------------------------------------------------------------------
 *
 * C++ interface to gnuplot
 *
 * By Ed Breen.
 *
 * Modified Hugues Talbot	28 Jan 1997
 *      
 *-----------------------------------------------------------------------*/

#ifndef GPLOT2_H
#define GPLOT2_H

#include <stdlib.h>
#include <stdio.h>
#include <list>
#include "imnmspc.hxx" // namespace def. if required
#include "machine.hxx"


struct plot_t {
    FILE *fp;
    char  fname[L_tmpnam + 1];
    /* the following are used for multiplots */
    int nrow;  /* number of rows */
    int ncol;  /* number of cols */
    int cp;    /* current plot in use */
};

/* plot methods */
#define getPlotncol(x)  (x)->ncol
#define getPlotnrow(x)  (x)->nrow
#define getCurrentPlot(x) (x)->cp
#define getPlotFile(x)   (x)->fp

#define setCurrentPlot(x,v)  (x)->cp = v

/*******************************/
#define send2plot        Send_2_plot_

#define replot(x)        Send_2_plot_(x,"replot")
#define titleplot(x,s)   Send_2_plot_(x,"set title '%s'",s)
#define xlabelplot(x,s)  Send_2_plot_(x,"set xlabel '%s'",s)
#define ylabelplot(x,s)  Send_2_plot_(x,"set ylabel '%s'",s)
#define zlabelplot(x,s)  Send_2_plot_(x,"set zlabel '%s'",s)




/* prototypes */
plot_t * openPlot(char * plotname);
void * closePlot(plot_t *p);
void Send_2_plot_(plot_t *p, const char * fmt, ...);
void plotDoubles(double *hist, int n,plot_t *p, bool smooth);
char *writeDoubleVector(double *vec, int n);
void dataMPlot(plot_t *p,
               char * (*f)(...),  /* return the name of the file where data is stored */
               int nv,         /* number of vectors in vecs */
               void **vecs,    /* array of vectors */
	       const char **labels, /* labels */
               int *lens,   /* array of lengths to vecs */   
	       bool smooth);    /* smooth or not ?*/
#endif //GPLOT2_H
