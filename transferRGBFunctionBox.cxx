/*
 * $Id: transferRGBFunctionBox.cxx,v 4.0 2003/04/28 14:40:17 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * A subclass of Box for drawing transfer functions
 *
 * Hugues Talbot	 3 May 1998
 *      
 *-----------------------------------------------------------------------*/

#include <math.h>

#include "imview.hxx"
#include "imageIO.hxx"
#include "imageViewer.hxx"
#include "transferBox.hxx"
#include "transferRGBFunction.hxx"
#include "transferRGBFunctionBox.hxx"

extern imageIO *IOBlackBox;
extern imageViewer *mainViewer;
extern transferRGB  *transferRGBEditorPanel;


void myTransferRGBFunctionBox::draw()
{
    int startX, startY, endX, endY, inc;
    double x0[3], y0[3], x1[3], y1[3], swapd;  // these are normally between 0 and 1
    int    w0, h0;
    int    xa, ya, xb, yb;

 // call the superclass' draw routine.
    myTransferRGBBox::draw();

    computeRGBEndPoints(x0, y0, x1, y1); // this returns the End points.
    
    fl_push_no_clip();
    //fl_clip(x(),y(),w(),h());
    for (int i = 0 ; i < 3 ; ++i) {
	//fl_clip(x(),y(),w(),h());
	switch (i) {
	case 0:
	    fl_color(FL_RED);
	    break;
	case 1:
	    fl_color(FL_GREEN);
	    break;
	case 2:
	    fl_color(FL_BLUE);
	    break;
	}
	inc = 1;

	if (x0[i] > x1[i] || y0[i] > y1[i]) { // negative contrast
	    swapd = x1[i];
	    x1[i] = x0[i];
	    x0[i] = swapd;
	    if (y0[i] > 1)  {
		startX = (int)(x0[i]*(w()-1));
		startY = 0;
	    } else {
		startX = 0;
		startY = (int)((h()-1)*(1-y0[i]));
	    }
	    if (y1[i] > 0) {
		endX = w()-1;
		endY = (int)((h()-1)*(1-y1[i]));
	    } else {
		endX = (int)(x1[i]*(w()-1));
		endY = h()-1;
	    }
	    dbgprintf("Start: (%d, %d) ; end: (%d, %d)\n",
		      startX, startY, endX, endY);
	    w0 = endX - startX;
	    h0 = startY - endY; // h is inverted...
	    xa = 0;
	    ya = 0;
	    if (w0 != 0) {
		for (xb = startX; xb <= endX ; xb += inc) {
		    //yb = startY - (xb-startX)*h0/w0;
		    yb = (int)(startY - h0*pow(((double)xb-startX)/w0,1/getCurrGamma(i)));
		    fl_line(xa+x(), ya+y(), xb+x(),yb+y());
		    xa = xb;
		    ya = yb;
		}
	    } else {
		fl_line(x(), y()+h()-1, startX+x(), y()+h()-1);
		fl_line(startX+x(), startY+y(), endX+x(), endY+y());
		xa = endX;
		ya = endY;
	    }
	    // finish the path
	    fl_line(xa+x(), ya+y(), w()-1+x(), y()+h()-1);
	} else {
	    if (y0[i] < 0)  {
		startX = (int)(x0[i]*(w()-1));
		startY = h()-1;
	    } else {
		startX = 0;
		startY = (int)((h()-1)*(1-y0[i]));
	    }
	    if (y1[i] <= 1) {
		endX = w()-1;
		endY = (int)((h()-1)*(1-y1[i]));
	    } else {
		endX = (int)(x1[i]*(w()-1));
		endY = 0;
	    }
	    dbgprintf("Start: (%d, %d) ; end: (%d, %d)\n",
		      startX, startY, endX, endY);
	    w0 = endX - startX;
	    h0 = startY - endY; // h is inverted...
	    xa = 0;
	    ya = h()-1;
	    if (w0 != 0) {
		for (xb = startX; xb <= endX ; xb += inc) {
		    //yb = startY - (xb-startX)*h0/w0;
		    yb = (int)(startY - h0*pow(((double)xb-startX)/w0,1/getCurrGamma(i)));
		    fl_line(xa+x(), ya+y(), xb+x(),yb+y());
		    xa = xb;
		    ya = yb;
		}
	    } else {
		fl_line(x(), y()+h()-1, startX+x(), y()+h()-1);
		fl_line(startX+x(), startY+y(), endX+x(), endY+y());
		xa = endX;
		ya = endY;
	    }
	    // finish the path
	    fl_line(xa+x(), ya+y(), w()-1+x(), y());
	}
    }
    fl_pop_clip();
    return;
}

