/*
 * $Id: imlines.hxx,v 4.2 2004/06/23 15:58:16 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * the imline class
 *
 * This class draws lines that can be easily erased on any background,
 * because they remember what was under them when they were first
 * drawn.
 *
 * Hugues Talbot	 4 Apr 1998
 *      
 *-----------------------------------------------------------------------*/

#ifndef IMLINES_H
#define IMLINES_H

#ifndef Fl_Object_H
#include <FL/Fl_Object.H>
#endif
#include <FL/Fl_Group.H>
#include <FL/fl_draw.H>
#include <FL/x.H>

#include <stdio.h>
#include <math.h>
//#include <deque>  // the STL queue container
// cannot be used with gcc 2.7.2...
#include <list>
#include "imnmspc.hxx" // contains namespace definition, if needed


using std::list;

#define  DEFAULT_XOR_FG       0xffffeffe

typedef enum {
    STYLE_XOR = 0,  // do not use, causes havoc with redraws
    STYLE_SPARSE_COLOUR,
    STYLE_FILLED_COLOUR,
    STYLE_FILLED_BW,
    STYLE_MAX
} linestyle;

struct impoint {
    unsigned char R, G, B;  // colour values
};

class imline:public Fl_Object {
public:
    imline(void);
    imline(const imline &l); // copy constructor...
    imline(int x1, int y1, int x2, int y2, 
           linestyle style=STYLE_XOR, int xorval=DEFAULT_XOR_FG, Fl_Color linecolour=FL_WHITE);
    ~imline(void);
    void draw(void);
    void undraw(void);
    void drawnStatus(int newstatus) { firstdrawn_ = (newstatus == 0); }
    bool drawnStatus(void) { return firstdrawn_; }
    void defaultXorValue(int xorval) {xorval_ = xorval;}
    void getExtremities(int &x1, int &y1, int &x2, int &y2) {
	x1 = x1_; y1 = y1_;
	x2 = x2_; y2 = y2_;
    }
    void select(bool v=true) {selected_ = v;}
    bool selected(void) const {return selected_;}
    void setcolour(Fl_Color c) {linecolour_ = c;}
private:
    int                 x1_, y1_, x2_, y2_;
    int                 style_;
    bool                firstdrawn_;
    list <impoint>      pointqueue;
    // bresenham
    int                 incx_, incy_, inc1_, inc2_, sizex_;
    int                 dx_, dy_, i_, e_;
    int                 xpos_, ypos_;
    void draw_xorline(void);
    void draw_filledLine(void);
    void draw_BWLine(void);
    int  bresinit(void);
    int  bresgetnext(void);
    // xor function
    int                 xorval_ ;
    bool                selected_; // joins two selected points
    // colour
    Fl_Color             linecolour_;
};


#endif // IMLINES_H
