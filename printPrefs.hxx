/*
 * $Id: printPrefs.hxx,v 4.4 2003/09/09 07:46:04 hut66au Exp $
 *
 * Imview, the portable image analysis application
 * http://www.cmis.csiro.au/Hugues.Talbot/imview
 * ----------------------------------------------------------
 *
 *  Imview is an attempt to provide an image display application
 *  suitable for professional image analysis. It was started in
 *  1997 and is mostly the result of the efforts of Hugues Talbot,
 *  Image Analysis Project, CSIRO Mathematical and Information
 *  Sciences, with help from others (see the CREDITS files for
 *  more information)
 *
 *  Imview is Copyrighted (C) 1997-2001 by Hugues Talbot and was
 *  supported in parts by the Australian Commonwealth Science and 
 *  Industry Research Organisation. Please see the COPYRIGHT file 
 *  for full details. Imview also includes the contributions of 
 *  many others. Please see the CREDITS file for full details.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA.
 * */

/*------------------------------------------------------------------------
 *
 * A printPreferenceWindow class
 *
 * Experimenting OOP with fluid
 *
 *      
 *-----------------------------------------------------------------------*/

#ifndef PRINTPREFS_H
#define PRINTPREFS_H

#include <stdio.h>
#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Return_Button.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Choice.H>


class printprefs {
public:
    printprefs();
    void setDefaults();
    void toggleToFile();
    void toggleToPrinter();
    void allowTitle(bool b);
    void setTitle(const char *title);
    void printFileName(const char *fn);
    const char *filenameInputValue(void) { return filenameInput->value(); }
    void setpapertype(int p) {papertype_ = p;}
    void setMargins(long whichMargin);
    void show();
    void hide();
    int doThePrinting(bool preview=false);
    friend Fl_Double_Window *printprefs_panel(printprefs &s);

private:
    // in the order they appear in fluid
    // -- The dialog window
    Fl_Double_Window    *prefWindow; // the main window

    // Print type group
    Fl_Group            *printtypeGroup;
    Fl_Check_Button     *printerCheck, *fileCheck;

    // Input group
    Fl_Group            *inputGroup;
    Fl_Input            *commandInput;
    Fl_Input            *filenameInput;
    Fl_Button           *browseButton;
    Fl_Input            *titleInput;

    // 1- Main parameter tab
    // Orientation Group
    Fl_Group            *orientationGroup;
    Fl_Check_Button     *portraitCheck, *landscapeCheck;

    // Paper size group
    Fl_Group            *papersizeGroup;
    // Replaced by an Fl_Choice
    // Fl_Check_Button     *A4Check, *letterCheck, *A3Check;
    Fl_Choice           *papersizeChoice;

    // Language Group
    Fl_Group            *languageGroup;
    Fl_Check_Button     *psCheck, *epsCheck;

    Fl_Input            *nbcopiesInput;

    // fit to page group
    Fl_Group            *fitGroup;
    Fl_Check_Button     *oneImageCheck, *wholeListCheck;
    
    // Subset group
    Fl_Group            *subsetGroup;
    Fl_Check_Button     *wholeCheck, *subsetCheck;
    
    // 2- Margin tab
    //
    Fl_Choice           *marginDims;
    Fl_Menu_Item        *standardMarginItem, *smallMarginItem;
    Fl_Menu_Item        *largeMarginItem, *zeroMarginItem;
    Fl_Menu_Item        *customMarginItem;

    Fl_Input            *leftMarginInput, *rightMarginInput;
    Fl_Input            *topMarginInput, *bottomMarginInput;
    Fl_Input            *titleMarginInput;

    Fl_Return_Button    *OKButton;
    Fl_Button           *CancelButton;
    Fl_Button           *PreviewButton;    

    // other data
    int                  papertype_;
};



#endif // PRINTPREFS_H
